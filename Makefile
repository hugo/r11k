.PHONY: check test doc docserver

PYTHON = python3
FLAKE8 = flake8
MYPY = mypy
PYDOCSTYLE = pydocstyle


MYPY_FLAGS=
ifeq ($(CI),true)
	MYPY_FLAGS = --html-report htmlrep
else
	MYPY_FLAGS = --pretty
endif

check:
	$(MYPY) $(MYPY_FLAGS) -p r11k -p tests
	$(FLAKE8) r11k tests
	$(PYDOCSTYLE) r11k

BRANCH = $(shell git branch --show-current 2>/dev/null)
PDOC_FLAGS = \
		--edit-url r11k=https://git.lysator.liu.se/hugo/r11k/-/blob/$(BRANCH)/r11k/ \
		--search \
		--show-source

doc:
	pdoc --output-directory doc $(PDOC_FLAGS) ./r11k

docserver:
	pdoc $(PDOC_FLAGS) ./r11k

PYTEST_COV_FLAGS = --cov=r11k --cov-branch --cov-report=html
PYTEST_FLAGS = $(PYTEST_COV_FLAGS)

# CI set by gitlab-ci
ifeq ($(CI),true)
	PYTEST_FLAGS+=-v
endif

test:
	$(PYTHON) -m pytest $(PYTEST_FLAGS)

venv:
	$(PYTHON) -m venv --symlinks --prompt r11k venv

tags:
	ctags --exclude=.mypy_cache --recurse r11k
