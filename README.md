- Given a `Puppetfile` (`.yaml`)
    - download all metadata to a temporary directory
    - try to resolve the dependency graph
    - (possibly) commit to the real directory
    - (optionally) check OS compatibility

Other stuff
- check if current metadata-file is valid (unless puppet already does that)
- check if stuff can be updated
