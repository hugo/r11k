from r11k.puppet import (
    DependencySpec,
)

from r11k.interval import Interval


def test_dependency_spec():
    ds = DependencySpec("module-name", ">= 1.0.0 < 2.0.0")
    assert isinstance(ds.interval('other-module'), Interval)
