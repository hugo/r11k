import operator
from semver import VersionInfo
import pytest
from r11k.interval import (
    Interval,
    __join_opt_strings,
    intersect,
    overlaps,
    parse_interval,
)


def test_join_opt_strings():
    assert None is __join_opt_strings(None, None)
    assert 'a' == __join_opt_strings(None, 'a')
    assert 'a,b' == __join_opt_strings('a', 'b')


# Test parsing of intervals, used by later tests for constructing
# intervals.

def test_parse_interval_1():
    interval = parse_interval('[1.0.0, 2.0.0)')
    assert interval.minimi == VersionInfo(1, 0, 0)
    assert interval.maximi == VersionInfo(2, 0, 0)
    assert interval.min_cmp == operator.ge
    assert interval.max_cmp == operator.lt
    assert interval.min_by == ''
    assert interval.max_by == ''


def test_parse_interval_2():
    interval = parse_interval('(0.0.0, 1.2.3]')
    assert interval.minimi == VersionInfo(0, 0, 0)
    assert interval.maximi == VersionInfo(1, 2, 3)
    assert interval.min_cmp == operator.gt
    assert interval.max_cmp == operator.le


def test_parse_interval_invalid_start():
    with pytest.raises(ValueError):
        parse_interval('a1.0.0, 2.0.0]')


def test_parse_interval_invalid_end():
    with pytest.raises(ValueError):
        parse_interval('[1.0.0, 2.0.0e')


def test_parse_interval_invalid_v1():
    with pytest.raises(ValueError):
        parse_interval('[1.0, 2.0.0]')


def test_parse_interval_invalid_v2():
    with pytest.raises(ValueError):
        parse_interval('[1.0.0, 2.0]')


# Comparison of intervals
# At one point two identical (but differently allocated) intervals
# wheren't equal
def test_interval_equality():
    interval_1 = parse_interval('[1.0.0, 2.0.0)')
    interval_2 = parse_interval('[1.0.0, 2.0.0)')
    assert interval_1 == interval_2


# Test intersections of intervals

def test_self_intersect():
    interval = parse_interval('[1.0.0, 2.0.0)')
    assert interval == intersect(interval, interval)


def test_simple_intersection():
    interval1 = parse_interval('[1.0.0, 3.0.0]')
    interval2 = parse_interval('[2.0.0, 4.0.0]')
    interval3 = parse_interval('[1.0.0, 4.0.0]')
    assert interval3 == intersect(interval1, interval2)


def test_exact_intersection():
    interval1 = parse_interval('[1.0.0, 2.0.0]')
    interval2 = parse_interval('[2.0.0, 4.0.0]')
    interval3 = parse_interval('[1.0.0, 4.0.0]')
    assert interval3 == intersect(interval1, interval2)


def test_non_overlapping():
    interval1 = parse_interval('[1.0.0, 2.0.0]')
    interval2 = parse_interval('[3.0.0, 4.0.0]')
    with pytest.raises(ValueError):
        assert intersect(interval1, interval2)


def test_nonintersection():
    interval1 = parse_interval('[1.0.0, 2.0.0)')
    interval2 = parse_interval('(2.0.0, 4.0.0]')
    with pytest.raises(ValueError):
        intersect(interval1, interval2)


def test_invalid():
    with pytest.raises(ValueError):
        parse_interval('[2.0.0, 1.0.0]')


def test_overlaps_1():
    assert not overlaps(parse_interval("[1.0.0, 2.0.0)"),
                        parse_interval("(2.0.0, 3.0.0]"))
    assert not overlaps(parse_interval("(2.0.0, 3.0.0]"),
                        parse_interval("[1.0.0, 2.0.0)"))


def test_overlaps_2():
    assert not overlaps(parse_interval("[1.0.0, 2.0.0)"),
                        parse_interval("[2.0.0, 3.0.0]"))
    assert not overlaps(parse_interval("[2.0.0, 3.0.0]"),
                        parse_interval("[1.0.0, 2.0.0)"))


def test_overlaps_3():
    assert not overlaps(parse_interval("[1.0.0, 2.0.0]"),
                        parse_interval("(2.0.0, 3.0.0]"))
    assert not overlaps(parse_interval("(2.0.0, 3.0.0]"),
                        parse_interval("[1.0.0, 2.0.0]"))


def test_overlaps_4():
    assert overlaps(parse_interval("[1.0.0, 2.0.0]"),
                    parse_interval("[2.0.0, 3.0.0]"))
    assert overlaps(parse_interval("[2.0.0, 3.0.0]"),
                    parse_interval("[1.0.0, 2.0.0]"))


def test_overlaps_5():
    assert overlaps(parse_interval("[1.0.0, 3.0.0]"),
                    parse_interval("[2.0.0, 4.0.0]"))


def test_overlaps_6():
    assert overlaps(parse_interval("[2.0.0, 3.0.0]"),
                    parse_interval("[1.0.0, 2.0.0]"))


def test_overlaps_7():
    assert overlaps(parse_interval("[1.0.0, 4.0.0]"),
                    parse_interval("[2.0.0, 3.0.0]"))


def test_overlaps_8():
    assert overlaps(parse_interval("[2.0.0, 3.0.0]"),
                    parse_interval("[1.0.0, 4.0.0]"))


def test_overalps_9():
    assert not overlaps(parse_interval("[1.0.0, 2.0.0]"),
                        parse_interval("[3.0.0, 4.0.0]"))


def test_invalid_constructor():
    with pytest.raises(ValueError):
        Interval(VersionInfo(0), VersionInfo(0),
                 operator.eq, operator.lt)

    with pytest.raises(ValueError):
        Interval(VersionInfo(0), VersionInfo(0),
                 operator.gt, operator.eq)


def test_eq():
    i1 = parse_interval('[1.0.0, 2.0.0)')
    i2 = parse_interval('[1.0.0, 2.0.0)')
    assert i1 == i2


def test_neq():
    i1 = parse_interval('[1.0.0, 2.0.0)')
    i2 = parse_interval('[1.0.0, 2.0.1)')
    assert i1 != i2


def test_eq_non_interval():
    assert parse_interval('[1.0.0, 2.0.0)') != 5


def test_newest():
    interval = parse_interval('[1.0.0, 2.0.0)')
    newest = interval.newest([VersionInfo(1, 0, 0),
                              VersionInfo(1, 2, 3),
                              VersionInfo(2, 0, 0)])
    assert newest == VersionInfo(1, 2, 3)


def test_no_newest():
    interval = parse_interval('[1.0.0, 2.0.0)')
    with pytest.raises(ValueError):
        interval.newest([VersionInfo(2, 0, 0)])


def test_repr():
    i = parse_interval('[1.0.0, 2.0.0)')
    assert eval(repr(i)) == i


def test_str():
    i = parse_interval('[1.0.0, 2.0.0)')
    assert isinstance(str(i), str)
