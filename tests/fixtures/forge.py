import json
import os
import hashlib

from datetime import datetime, timezone, timedelta
from pathlib import Path
from typing import (
    Optional,
    cast,
)

import pytest
import pytest_httpserver
from tarfile import (
    TarFile,
)

from r11k.puppet import PuppetMetadata, DependencySpec
from r11k.forge import (
    CurrentRelease,
    ForgeModule,
    FullForgeModule,
    Owner,
    Release,
)

from tests.util.encoder import Encoder


# Helpers for generating fake forge data

def make_owner(owner: str) -> Owner:
    """Generate dummy forge owner object."""
    return {'uri': f'/v3/users/{owner}',
            'slug': owner,
            'username': owner,
            'gravatar_id': 'NOT AVAILABLE'}


CET = timezone(timedelta(hours=1))

Author = str
ModuleName = str
ModuleVersion = str


# Fake forge data

class ModuleRelease:
    """
    Single release of puppet module.

    The below mentioned fields needs to be explicitly set after the
    constructor is called, but before most other methods are used.

    [Fields]:
        tar_dir - Where the directory to pack will be created, along
                  with the resulting archive.
        author - Name of the module author, such as 'hugonikanor'
        name - Name of the module, such as 'stdlib'
    """
    def __init__(self,
                 version: ModuleVersion,
                 *,
                 deps: Optional[list[DependencySpec]] = None,
                 files: Optional[dict[str, str]] = None):
        """
        [Parameters]
            version - Semver string
            deps - List of modules we depend on
            files - dictionary from filename to file content
        """
        self.version = version
        self.deps: list[DependencySpec] = deps or []
        self._files: dict[str, str] = files or {}

        self.author: Author
        self.name: ModuleName
        self.tar_dir: str
        self._tar_archive: Optional[str] = None

    def files(self) -> dict[str, str]:
        """
        Return given files, plus generated metadata.json.

        Used as content of tar_archive below.
        """
        return self._files \
            | {'metadata.json':
               json.dumps(self.metadata(), cls=Encoder, indent=3)}

    def tar_uri(self):
        """
        Return API endpoint for modules tarball.

        Requires that author, and vame is set.
        """
        return f'/v3/files/{self.author}-{self.name}-{self.version}.tar.gz'

    @property
    def tar_archive(self) -> str:
        """Create a tar archive on disk on first access, always return its path."""
        if self._tar_archive:
            return self._tar_archive

        tar_root = f'{self.author}-{self.name}-{self.version}'
        """
        Used for root directory of tar archive (since proper archives
        contain exactly one top level directory). Also used for
        resulting filename.
        """

        try:
            os.mkdir(self.tar_dir)
        except FileExistsError:
            pass

        root = os.path.join(self.tar_dir, tar_root)
        os.mkdir(root)

        for name, content in self.files().items():
            with open(os.path.join(root, name), 'w') as f:
                f.write(content)

        # TODO change this to a non-standard compression, to check that
        # the code also handles that.
        # This would also require an update to tar_uri above, along
        # with us actually checking what to download instead of
        # "hard-coding" the name.
        path = os.path.join(self.tar_dir, f'{tar_root}.tar.gz')
        with TarFile.open(name=path, mode='w:gz') as tf:
            tf.add(root, arcname=tar_root)

        self._tar_archive = path
        return path

    def metadata(self) -> PuppetMetadata:
        """Return metadata for this module."""
        source_url = f'https://example.com/{self.author}/{self.author}-{self.name}'
        return PuppetMetadata(name=f'{self.author}-{self.name}',
                              version=self.version,
                              author=self.author,
                              summary='Test module for R11K',
                              license='License text would have gone here',
                              source=source_url,
                              dependencies=self.deps)

    def as_current_release(self) -> CurrentRelease:
        """Make dummy forge release object."""
        with open(self.tar_archive, 'rb') as f:
            data = f.read()
            tar_length = len(data)
            sha256: str = hashlib.sha256(data).hexdigest()
            md5: str = hashlib.md5(data).hexdigest()

        return CurrentRelease(
            uri=f'/v3/releases/{self.author}-{self.name}-{self.version}',
            slug=f'{self.author}-{self.name}-{self.version}',
            module=ForgeModule(
                uri=f'/v3/modules/{self.author}-{self.name}',
                slug=f'{self.author}-{self.name}',
                name=self.name,
                deprecated_at=None,
                owner=make_owner(self.author)),
            version=self.version,
            metadata=self.metadata(),
            tags=[],
            pdk=False,
            validation_score=100,
            file_uri=self.tar_uri(),
            file_size=tar_length,
            file_md5=md5,
            file_sha256=sha256,
            downloads=0,
            readme='README Would have gone here',
            changelog='Changelog would have gone here',
            license='License text would have gone here',
            reference='ModuleRelease reference documentation would have gone here',
            tasks=[],
            plans=[],
            created_at=datetime(2022, 1, 1, 10, tzinfo=CET),
            updated_at=datetime(2022, 9, 15, 12, tzinfo=CET),
            deleted_at=None,
            deleted_for=None)


class FullModule:
    """
    A specific module, without a specific version, in the forge.

    Can be seen as the parent of a series of versions.
    """
    def __init__(self, name, releases: list[ModuleRelease]):
        self._author: Optional[Author]
        self.name: ModuleName = name
        self.releases = releases
        for release in self.releases:
            release.name = name

        self._tar_dir: str

    @property
    def author(self) -> Author:
        if not self._author:
            raise ValueError('Author not set')
        return self._author

    @author.setter
    def author(self, value: Author) -> None:
        self._author = value
        for release in self.releases:
            release.author = value

    @property
    def tar_dir(self) -> str:
        return self._tar_dir

    @tar_dir.setter
    def tar_dir(self, value: str) -> None:
        for release in self.releases:
            release.tar_dir = value
            assert release.tar_archive
        self._tar_dir = value

    def as_forge_module(self) -> FullForgeModule:
        """
        Create a dummy forge module entyr.

        Releases must be a list of current releases, instead of releases,
        to be able to fil out the current_release field.
        """
        homepage = f'https://example.com/{self.author}/{self.author}-{self.name}'
        return FullForgeModule(uri=f'/v3/modules/{self.author}-{self.name}',
                               slug=f'{self.author}-{self.name}',
                               name=self.name,
                               downloads=0,
                               created_at=datetime(2022, 1, 1, tzinfo=CET),
                               updated_at=datetime(2022, 12, 12, tzinfo=CET),
                               deprecated_at=None,
                               deprecated_for=None,
                               superseded_by=None,
                               endorsement=None,
                               module_group='base',
                               premium=False,
                               owner=make_owner(self.author),
                               current_release=max(self.releases,
                                                   key=lambda x: x.version).as_current_release(),
                               releases=[cast(Release, x.as_current_release())
                                         for x in self.releases],
                               feedback_score=100,
                               homepage_url=homepage,
                               issues_url=homepage)


class ModuleGroup:
    def __init__(self, author: Author, modules: list[FullModule]):
        self.author = author
        self.modules = modules

        for module in modules:
            module.author = author

    @property
    def tar_dir(self) -> str:
        raise ValueError("Can't get tar path")

    @tar_dir.setter
    def tar_dir(self, value: str) -> None:
        for module in self.modules:
            module.tar_dir = value


def forge_data(tar_dir: str) -> list[ModuleGroup]:
    """
    Returns data tailored for the forge fixture below.

    [Parameters]
        tar_dir - Where temporary tarballs to be "sent" should be created.
    """
    groups = [
        ModuleGroup('hugonikanor', [
            FullModule('testlib', [
                ModuleRelease('7.0.0', files={
                    'only-in-lower.txt': 'This is only part of 7.0.0',
                    'in-both.txt': 'This is in both versions',
                }),
                ModuleRelease('8.0.0', files={
                    'only-in-upper.txt': 'This is only part of 8.0.0',
                    'in-both.txt': 'This is the updated version',
                }),
            ]),
            FullModule('A', [
                ModuleRelease('1.0.0', deps=[DependencySpec('hugonikanor-B', '1.0.0')])
            ]),
            FullModule('B', [
                ModuleRelease('1.0.0', deps=[DependencySpec('hugonikanor-C', '1.0.0')]),
            ]),
            FullModule('C', [
                ModuleRelease('1.0.0'),
            ]),
        ]),
    ]

    for group in groups:
        group.tar_dir = tar_dir

    return groups


@pytest.fixture()
def forge(httpserver: pytest_httpserver.HTTPServer,
          tmp_path: Path) -> pytest_httpserver.HTTPServer:
    """
    Local puppet forge API.

    Data is populated from the two above defined dictionaries releases
    and modules.
    """

    tar_dir = os.path.join(tmp_path, 'tar-creation')

    for module_group in forge_data(tar_dir):
        author = module_group.author
        for module in module_group.modules:
            module_name = module.name
            httpserver.expect_request(f'/v3/modules/{author}-{module_name}') \
                      .respond_with_data(json.dumps(module.as_forge_module(), cls=Encoder),
                                         content_type='text/json')

            for module_release in module.releases:
                version = module_release.version
                httpserver.expect_request(f'/v3/releases/{author}-{module_name}-{version}') \
                          .respond_with_data(json.dumps(module_release.as_current_release(),
                                                        cls=Encoder),
                                             content_type='text/json')

                with open(module_release.tar_archive, 'rb') as f:
                    data = f.read()
                    httpserver.expect_request(module_release.tar_uri()) \
                              .respond_with_data(data)

    return httpserver
