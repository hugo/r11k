from pathlib import Path
import os
import os.path
import random
from typing import (
    Any,
    Callable,
    Optional,
    Tuple,
    TypeAlias,
)

import pytest
import pygit2
from tests.util.git import DEFAULT_BRANCH

try:
    # builtin fixture types started being exported in pytest 6.2.0.
    # Debian 11 ships 6.0.2.
    from pytest import FixtureRequest  # type: ignore
except ImportError:
    # The first Any should be TypeAlias, but that isn't either
    # available.
    FixtureRequest: Any = Any  # type: ignore


@pytest.fixture()
def git_upstream(request: FixtureRequest, tmp_path: Path) -> pygit2.Repository:
    """Return a new bare git repo."""
    path = os.path.join(tmp_path, 'upstream', f'{request.node.name}.git')
    repo = pygit2.init_repository(path, bare=True)
    return repo


UpstreamFactory: TypeAlias = Callable[[Optional[str]], pygit2.Repository]


@pytest.fixture()
def git_upstream_factory(request: FixtureRequest,
                         tmp_path: Path) -> UpstreamFactory:
    """Return a function returning a new bare git repo every time its called."""
    def new_repo(name: Optional[str] = None):
        # NOTE We technically have to check for collisions...
        repo_name = f'{name or request.node.name}-{random.randint(0, 10000)}.git'
        path = os.path.join(tmp_path, 'upstream', repo_name)
        repo = pygit2.init_repository(path, bare=True)
        return repo
    return new_repo


sample_author: pygit2.Signature = pygit2.Signature('Firstname Lastnameson',
                                                   'author@example.com')


@pytest.fixture()
def populated_git_upstream_factory(request: FixtureRequest,
                                   git_upstream_factory: UpstreamFactory,
                                   tmp_path: Path):
    """
    Return a function configuring a new git repo, which returns a upstream and downstream.

    The parameters will be used to construct a single inistial commit,
    which will be pushed to the upstream.
    """
    def new_populated_upstream(repo_name: str,
                               content: dict[str, bytes],
                               *,
                               author: pygit2.Signature = sample_author,
                               comitter: pygit2.Signature = sample_author,
                               ref='HEAD') -> Tuple[pygit2.Repository, pygit2.Repository]:
        """
        :param name: Name of the downstream, part of the name of
                     downstream, and used for the commit message.
                     Mostly for easy of debug.
        :param content: File contents of the commit. Keys are
                        filenames, values are file contents. Currently
                        only supports a root directory.
        :param author: Author signature to use, defaluts to `sample_author'.
        :param comitter: Author signature to use, defaluts to `sample_author'.
        :param ref: Reference to commit to, defaluts to 'HEAD', but
                    'refs/heads/{name}' is a good alternative.
        """

        work_repo_path = os.path.join(tmp_path, 'workdir', repo_name)
        work_repo = pygit2.init_repository(work_repo_path)

        for filename, file_content in content.items():
            with open(os.path.join(work_repo_path, filename), 'wb') as f:
                f.write(file_content)
            work_repo.index.add(filename)

        work_repo.create_commit(ref, author, comitter,
                                f'Initial commit {repo_name}',
                                work_repo.index.write_tree(),
                                [])

        upstream = git_upstream_factory(f'upstream-{repo_name}')
        origin = work_repo.remotes.create('origin', upstream.path)
        # TODO DEFAULT_BRANCH
        # TODO non-hacky way to push, or distinguish default ref.
        if ref == 'HEAD':
            origin.push([f'refs/heads/{DEFAULT_BRANCH}'])
        else:
            origin.push([ref])

        return work_repo, upstream

    return new_populated_upstream
