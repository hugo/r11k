import os.path

import pytest
from pathlib import Path
# from fixtures import forge
from pytest_httpserver import HTTPServer

import r11k.config


@pytest.fixture()
def testconf(forge: HTTPServer, tmp_path: Path) -> r11k.config.Config:
    """Alternative configuration containing our test Forge."""
    conf = r11k.config.Config(api_base=forge.url_for(''),
                              http_ttl=3600,
                              git_ttl=3600,
                              cache_root=os.path.join(tmp_path, 'cache'))
    return conf


@pytest.fixture()
def testconf_noforge(tmp_path: Path) -> r11k.config.Config:
    conf = r11k.config.Config(api_base='http://localhost:0',
                              http_ttl=3600,
                              git_ttl=3600,
                              cache_root=os.path.join(tmp_path, 'cache'))
    return conf
