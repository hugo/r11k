import operator
from semver import VersionInfo
import pytest
from r11k.version import (
    max_version,
    min_version,
    parse_version,
)


def test_all_components():
    """<a>.<b>.<c>"""
    cmp_a, val_a, cmp_b, val_b = parse_version('1.2.3')
    assert val_a == VersionInfo(1, 2, 3)
    assert val_b == VersionInfo(1, 2, 3)
    assert cmp_a == operator.ge
    assert cmp_b == operator.le


def test_only_first():
    """<a>.x"""
    cmp_a, val_a, cmp_b, val_b = parse_version('1.x')
    assert val_a == VersionInfo(major=1)
    assert val_b == VersionInfo(major=2)
    assert cmp_a == operator.ge
    assert cmp_b == operator.lt


def test_first_and_second():
    """<a>.<b>.x"""
    cmp_a, val_a, cmp_b, val_b = parse_version('1.2.x')
    assert val_a == VersionInfo(major=1, minor=2)
    assert val_b == VersionInfo(major=1, minor=3)
    assert cmp_a == operator.ge
    assert cmp_b == operator.lt


def test_le():
    """>= <a>.<b>.<c>"""
    cmp_a, val_a, cmp_b, val_b = parse_version('>= 1.2.3')

    assert cmp_a == operator.ge
    assert val_a == VersionInfo(1, 2, 3)

    assert cmp_b == operator.lt
    assert val_b == max_version


def test_ge():
    """<= <a>.<b>.<c>"""
    cmp_a, val_a, cmp_b, val_b = parse_version('<= 1.2.3')

    assert cmp_a == operator.gt
    assert val_a == min_version

    assert cmp_b == operator.le
    assert val_b == VersionInfo(1, 2, 3)


def test_lt():
    """> <a>.<b>.<c>"""
    cmp_a, val_a, cmp_b, val_b = parse_version('> 1.2.3')

    assert cmp_a == operator.gt
    assert val_a == VersionInfo(1, 2, 3)
    assert cmp_b == operator.lt
    assert val_b == max_version


def test_gt():
    """< <a>.<b>.<c>"""
    cmp_a, val_a, cmp_b, val_b = parse_version('< 1.2.3')
    assert cmp_a == operator.gt
    assert val_a == min_version
    assert cmp_b == operator.lt
    assert val_b == VersionInfo(1, 2, 3)


def test_ge_lt():
    """>= <a>.<b>.<c> < <d>.<e>.<f>"""
    cmp_a, val_a, cmp_b, val_b = parse_version('>= 1.2.3 < 2.0.4')
    assert cmp_a == operator.ge
    assert val_a == VersionInfo(1, 2, 3)
    assert cmp_b == operator.lt
    assert val_b == VersionInfo(2, 0, 4)


def test_invalid():
    # Only slightly invalid, but enough (and also extra probable).
    with pytest.raises(ValueError):
        parse_version("1.0")
