from r11k.puppetmodule import (
    ForgePuppetModule,
    GitPuppetModule,
    parse_module_dict,
)

from tests.util.git import DEFAULT_BRANCH


def test_parse():
    assert isinstance(parse_module_dict({'name': 'puppetlabs-stdlib',
                                         'version': '8.1.0'}),
                      ForgePuppetModule)
    git_data = {'name': 'blog',
                'git': 'https://example.com/blog',
                'version': DEFAULT_BRANCH}
    assert isinstance(parse_module_dict(git_data),
                      GitPuppetModule)
