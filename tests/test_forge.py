from r11k.forge import _parse_date

from datetime import datetime, timezone, timedelta


def test_parse_date():
    dt = datetime(year=2022, month=9, day=16,
                  hour=23, minute=35, second=7,
                  tzinfo=timezone(timedelta(hours=2)))
    assert dt == _parse_date('2022-09-16 23:35:07 +02:00')
