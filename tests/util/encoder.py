from json import JSONEncoder
from datetime import datetime
from typing import (
    Optional,
    Any,
)
from semver import VersionInfo


# Helpers for serializing objects

def dump_date(d: Optional[datetime]) -> Optional[str]:
    """Serialize date to format puppet want it on."""
    if not d:
        return None
    s = d.strftime('%Y-%m-%d %H:%M:%S')
    if d.tzinfo:
        s += d.strftime(' %z')
    return s


class Encoder(JSONEncoder):
    """Custom JSON serializer handling datetimes, and anything implementing __dict__."""

    def default(self, o: Any) -> Any:
        """Serialize datetimes and anything implementing __dict__."""
        if isinstance(o, datetime):
            return dump_date(o)
        elif isinstance(o, VersionInfo):
            return str(o)
        # elif hasattr(o, 'serialize'):
        #     return o.serialize()
        else:
            return o.__dict__
