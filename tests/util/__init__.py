"""Misc utilities useful in tests."""

import os.path
from typing import Literal, Tuple


def check_dir(directory: str, expected: dict[str, bytes]) -> Literal[True]:
    """
    Ensure that a directory looks as expected.

    [Parameters]:
        directory - The directory to check
        expected - A dict from base filenames, to their SHA1sum
                   The value may also be the string 'IGNORE', in
                   which case that file isn't checked
    """
    filenames = os.listdir(directory)
    # Actual and expected filenamse must match exactly
    s_filenames = set(filenames)
    s_expected = set(expected)
    if s_filenames != s_expected:
        surplus = s_filenames - s_expected
        missing = s_expected - s_filenames
        raise ValueError(f"extra: '{surplus}', missing: '{missing}'")

    differing_files: list[Tuple[str, bytes, bytes]] = []
    for filename in filenames:
        if expected[filename] == b'IGNORE':
            continue
        path = os.path.join(directory, filename)
        if os.path.isfile(path):
            with open(path, 'rb') as f:
                data = f.read()
                if expected[filename] != data:
                    differing_files.append((filename, expected[filename], data))
        else:
            raise NotImplementedError(f'Non-standard files not yet suported: {path}')

    if differing_files:
        for name, exp, actual in differing_files:
            print(f'Expected content of {name}:')
            print(exp.decode('ASCII', errors='replace'))
            print(f'Actual content of {name}:')
            print(actual.decode('ASCII', errors='replace'))
        raise ValueError(f"The following files differ: {[x[0] for x in differing_files]}")

    return True
