import pytest

import os
import os.path
import json
import logging

from semver import VersionInfo
import pygit2
from pygit2 import GIT_RESET_HARD, GIT_OBJ_COMMIT

from r11k.puppetmodule.git import GitPuppetModule
from r11k.puppet import PuppetMetadata
from tests.util import check_dir
from tests.util.encoder import Encoder
from tests.util.git import DEFAULT_BRANCH


class TestGitStuff:
    def test_publish_git(self, testconf_noforge, tmp_path, populated_git_upstream_factory):
        """
        Test basic publish.

        - Setup
          - Create local repository
          - Initialize it with some data
          - Push it to remote gotten from fixture
        - Exec
          - Creates a single GitPuppetModule
          - Publish it to directory
          - Check that published data matches commited data
        """
        content = b'Content'
        _, upstream = populated_git_upstream_factory('upstream', {'file': content})

        # Load from remote as if from puppetfile
        module = GitPuppetModule(
            name='HugoNikanor-gittest',
            version=DEFAULT_BRANCH,
            git=upstream.path,
            config=testconf_noforge)

        # publish module to target
        path_base = os.path.join(tmp_path, 'target', 'modules', 'gittest')
        # Test add from nothing
        expected = {
            'file': content,
            '.git': b'IGNORE',
        }

        # Published data is same as commited data
        module.publish(path_base)
        assert check_dir(path_base, expected)

        # A republish without an updated upsteram is idempotent
        module.publish(path_base)
        assert check_dir(path_base, expected)

    def test_publish_weird_branch(self, testconf_noforge, tmp_path, populated_git_upstream_factory):
        """
        Test publish when default branch is missing.

        We initialize the upstream repo with a single branch: 'devel'
        (this assumes git's default branch isn't called 'devel'). Note
        that no 'master' (or similar) branch exists.

        It then initializes a GitPuppetModule as in test_publish_git,
        publishes that, and checks that the content matches.

        This has at one point failed due to the publish method not
        checking which version to publish.
        """

        # There are four instances of the repo in play here:
        # - BARE The upstream (from the fixture). This is a central git
        #        server or similar.
        # - Our local working copy, which is what we setup directly below
        # - BARE The clone cache, internal to r11k
        # - The final published repo

        content = b'Contents'

        _, git_upstream = populated_git_upstream_factory(
            'weird',
            {'file': content},
            ref='refs/heads/devel')

        # Sets up repo in cache
        module = GitPuppetModule(name='gittest2',
                                 version='devel',
                                 git=git_upstream.path,
                                 config=testconf_noforge)

        path_base = os.path.join(tmp_path, 'target', 'modules', 'gittest2')
        expected = {
            'file': content,
            '.git': b'IGNORE',
        }

        # Test add from nothing
        module.publish(path_base)
        assert check_dir(path_base, expected), "First check"
        # Test update
        module.publish(path_base)
        assert check_dir(path_base, expected), "Second check"

    def test_git_versions(self, testconf_noforge, tmp_path):
        """
        Check that all tags which are semver-tags gets reported.

        A dedicated upstream isn't used here, since a checked out
        repository still works as a remote, and we are only
        enumerating tags.
        """
        repo_path = os.path.join(tmp_path, 'workdir', 'versiontest')

        # Creates our test repository
        repo = pygit2.init_repository(repo_path)
        commit = repo.create_commit(
            f'refs/heads/{DEFAULT_BRANCH}',
            pygit2.Signature('Sample Author', 'author@example.com'),
            pygit2.Signature('Sample Commiter', 'commiter@example.com'),
            'Initial commit',
            repo.index.write_tree(),
            [])

        def create_tag(name, commit):
            repo.create_tag(name,
                            commit,
                            pygit2.GIT_OBJ_COMMIT,
                            pygit2.Signature('Sample Author', 'author@example.com'),
                            '')

        create_tag('v1.0.0', commit)
        create_tag('v1.0.1', commit)   # Multiple tags on one commit
        commit = repo.create_commit(
            f'refs/heads/{DEFAULT_BRANCH}',
            pygit2.Signature('Sample Author', 'author@example.com'),
            pygit2.Signature('Sample Commiter', 'commiter@example.com'),
            'Second commit',
            repo.index.write_tree(),
            [commit])
        create_tag('2.0.0', commit)  # 'v' is optional for semver git tags
        create_tag('3.0', commit)  # not semver

        # Load it as if from puppet file.
        module = GitPuppetModule(
            name='HugoNikanor-gittest',
            version='',   # Sentinel value, module only used to find available versions.
            git=repo_path,
            config=testconf_noforge)

        assert module.versions() == [VersionInfo.parse(x) for x in
                                     ['1.0.0',
                                      '1.0.1',
                                      '2.0.0',
                                      ]]

    def test_hash_version(self, testconf_noforge, tmp_path, populated_git_upstream_factory):
        """
        Test specifiying hash as version.

        Creates a repo with 2 commits, 2 being a child of 1:
        * 2
        * 1
        Then publish commit 1 (by hash) and check if it contains the
        expected data.

        Currently commit hash is gotten from commit object, but
        specifying commiting and auhoring date in `sig` would allow a
        stable hash, allowing us to hard code the hash.
        """

        downstream, upstream = populated_git_upstream_factory(
            'hashversion',
            {'file': b'1'})

        commit1 = downstream.head.peel(GIT_OBJ_COMMIT)

        with open(os.path.join(downstream.path, 'file'), 'wb') as f:
            f.write(b'2')
        downstream.index.add('file')

        sig = pygit2.Signature('Sample Author', 'author@example.com')
        downstream.create_commit('HEAD', sig, sig,
                                 'Second commit',
                                 downstream.index.write_tree(),
                                 [commit1.id])

        origin = downstream.remotes['origin']
        origin.push([f'refs/heads/{DEFAULT_BRANCH}'])

        module = GitPuppetModule(name='Anything',
                                 version=str(commit1.id),
                                 git=upstream.path,
                                 config=testconf_noforge)

        pub_dir = os.path.join(tmp_path, 'target', 'modules', 'anything')
        module.publish(pub_dir)

        with open(os.path.join(pub_dir, 'file'), 'rb') as f:
            assert b'1' == f.read()

    def test_git_publish_diverged(self, testconf_noforge, tmp_path, git_upstream):
        """
        Test publish of diverged branches.

        Sets up a local repo, which we commit to, then pushes that,
        and publishes it. The local repo then rewrites its history and
        force pushes, followed by us publishing the new data.

        * new master
        | * old master
        |/
        * common ancestor
        ...
        """
        publish_path = os.path.join(tmp_path, 'target', 'modules', 'diverged')
        module = GitPuppetModule(name='diverged',
                                 version=DEFAULT_BRANCH,
                                 git=git_upstream.path,
                                 config=testconf_noforge)

        sig = pygit2.Signature('Sample Author', 'author@example.com')

        repo_path = os.path.join(tmp_path, "workdir", "diverging")
        repo = pygit2.init_repository(repo_path)

        origin = repo.remotes.create('origin', git_upstream.path)

        file = os.path.join(repo_path, 'file')
        with open(file, 'wb') as f:
            f.write(b'1\n')
        repo.index.add('file')
        ancestor = repo.create_commit('HEAD', sig, sig, '1',
                                      repo.index.write_tree(),
                                      [])

        with open(file, 'wb') as f:
            f.write(b'2\n')
        repo.index.add('file')
        repo.create_commit('HEAD', sig, sig, '2',
                           repo.index.write_tree(),
                           [ancestor])

        # First check
        origin.push([f'refs/heads/{DEFAULT_BRANCH}'])
        module.publish(publish_path)
        with open(os.path.join(publish_path, file), 'rb') as f:
            assert b'2\n' == f.read()
        # end first check

        repo.reset(ancestor, GIT_RESET_HARD)

        with open(file, 'wb') as f:
            f.write(b'3\n')
        repo.index.add('file')
        repo.create_commit('HEAD', sig, sig, '3',
                           repo.index.write_tree(),
                           [ancestor])

        # Second check
        # leading '+' allows non-fastforward update (force push)
        origin.push([f'+refs/heads/{DEFAULT_BRANCH}'])
        module.publish(publish_path)
        with open(os.path.join(publish_path, file), 'rb') as f:
            assert b'3\n' == f.read()
        # end second check

    def test_git_metadata(self, testconf_noforge, tmp_path, populated_git_upstream_factory):
        """
        Checks that we can retrieve metadata.json from a git repo.

        Also tests that our git procedures handles version='1.0.0',
        when the actual tag is 'v1.0.0'
        """
        owner = 'hugonikanor'
        name = 'mdtest'
        version = 'v1.0.0'
        metadata = PuppetMetadata(name=f'{owner}-{name}',
                                  version=version,
                                  author=owner,
                                  summary='',
                                  license='',
                                  source='',
                                  dependencies=[])

        _, git_upstream = populated_git_upstream_factory(
            'versiontest',
            {'metadata.json': json.dumps(metadata, cls=Encoder).encode('ASCII')})

        # It doesn't matter if tag is created downstream and pushed,
        # or upstream directly. Upstream if fewer instructions.
        git_upstream.create_tag('v1.0.0',
                                git_upstream.head.peel(GIT_OBJ_COMMIT).id,
                                pygit2.GIT_OBJ_COMMIT,
                                pygit2.Signature('Sample Author', 'author@example.com'),
                                '')

        # We are done with our local repo, fetch it again from upstream

        module = GitPuppetModule(name=f'{owner}-{name}',
                                 version=version,
                                 git=git_upstream.path,
                                 config=testconf_noforge)

        assert module.metadata
        assert isinstance(module.metadata, PuppetMetadata)

        # Converting to JSON is the simplest way to compare
        # dictionaries without __eq__.
        # Local metadata
        ld_json = json.dumps(metadata, cls=Encoder)
        # Foreign metadata
        fd_json = json.dumps(module.metadata, cls=Encoder)
        assert ld_json == fd_json

    def test_missing_metadata(self, tmp_path, testconf_noforge, populated_git_upstream_factory):
        _, git_upstream = populated_git_upstream_factory('missing_metadata', {})

        module = GitPuppetModule(name='anything',
                                 version=DEFAULT_BRANCH,
                                 git=git_upstream.path,
                                 config=testconf_noforge)
        assert module.metadata
        assert isinstance(module.metadata, PuppetMetadata)

    def test_empty_repo(self, testconf_noforge, git_upstream, caplog):
        """Ensure a warning is raised when remote is empty."""

        caplog.set_level(logging.WARNING)

        module = GitPuppetModule(name='HugoNikanor-empty',
                                 version=DEFAULT_BRANCH,
                                 git=git_upstream.path,
                                 config=testconf_noforge)

        # This forces the repo to be cloned, exposing if it's empty.
        assert module.repo

        assert len(caplog.records) == 1
        # assert caplog.records[0].module == 'r11k.puppetmodule.git'
        assert caplog.records[0].levelname == 'WARNING'
        assert caplog.records[0].message.startswith('Empty repository cloned')

    def test_existing_local_repo_different(
            self,
            tmp_path,
            populated_git_upstream_factory,
            testconf_noforge):
        """
        Checks the case where the local repo (as returned by .repo())
        already exists, and refers to a DIFFERENT upstream.
        """

        # --------------------------------------------------

        _, upstream_a = populated_git_upstream_factory('a', {'filename.txt': b'a'})
        _, upstream_b = populated_git_upstream_factory('b', {'filename.txt': b'b'})

        # --------------------------------------------------

        dest = os.path.join(tmp_path, 'target', 'testrepo')

        module_a = GitPuppetModule(name='testrepo',
                                   version=DEFAULT_BRANCH,
                                   git=upstream_a.path,
                                   config=testconf_noforge)

        module_a.publish(dest)
        assert check_dir(dest, {
            'filename.txt': b'a',
            '.git': b'IGNORE',
        })

        module_b = GitPuppetModule(name='testrepo',
                                   version=DEFAULT_BRANCH,
                                   git=upstream_b.path,
                                   config=testconf_noforge)

        module_b.publish(dest)
        assert check_dir(dest, {
            'filename.txt': b'b',
            '.git': b'IGNORE',
        })

    def test_serialize(self, tmp_path, testconf_noforge, populated_git_upstream_factory):
        """
        Check that a GitPuppetModule serializes correctly for trace.

        Each PuppetModule should serialize to the exact version used,
        allowing us to produce a new puppetfile which exactly
        recreates a given deployment.
        """

        work_repo, git_upstream = populated_git_upstream_factory('repo', {})

        module = GitPuppetModule(name='repo',
                                 version=DEFAULT_BRANCH,
                                 git=git_upstream.path,
                                 config=testconf_noforge)

        serialized = module.serialize()

        # Expected keys in serialized output, change in the future.
        assert set(serialized) == set(['name', 'git', 'version'])

        assert serialized.get('name') == 'repo'

        # Output version should be the exact commit published, even
        # though the input gave a "named" version
        assert serialized.get('version') == work_repo.head.peel(GIT_OBJ_COMMIT).hex

    @pytest.mark.xfail(reason="What to do when publishing to existing non-git repo is not yet decided")  # noqa: E501
    def test_to_non_git_module(self, tmp_path, testconf_noforge, populated_git_upstream_factory):
        """
        Runs publish into a directory which already is a non-git module.
        """
        dest = os.path.join(tmp_path, 'target', 'module')
        os.makedirs(dest, exist_ok=True)

        with open(os.path.join(dest, 'file'), 'w') as f:
            print('non-git content', file=f)

        _, upstream = populated_git_upstream_factory('repo', {'file': b'from git'})
        module = GitPuppetModule(name='module',
                                 version=DEFAULT_BRANCH,
                                 git=upstream.path,
                                 config=testconf_noforge)

        # This crashes (as expected)
        module.publish(dest)

    def test_to_modified_module(self, tmp_path, testconf_noforge, populated_git_upstream_factory):
        """
        Runs publish into a correctly configured repository, but local
        (un-commited changes have been made).

        TODO do we also need a case for commited local changes?
        """
        _, upstream = populated_git_upstream_factory('repo', {'file': b'from git'})

        module = GitPuppetModule(name='module',
                                 version=DEFAULT_BRANCH,
                                 git=upstream.path,
                                 config=testconf_noforge)

        dest = os.path.join(tmp_path, 'target', 'module')
        module.publish(dest)

        #

        # Check if the local change is a non-tracked file
        with open(os.path.join(dest, 'new-file'), 'wb') as f:
            f.write(b'some content')
        module.publish(dest)
        assert check_dir(dest, {
            'file': b'from git',
            'new-file': b'some content',
            '.git': b'IGNORE',
        })

        # Check if the local change is a tracked file
        # This works if we run 'git checkout --force', but not
        # otherwise. Contemplate what is better, or if we could get a
        # warning here.
        with open(os.path.join(dest, 'file'), 'w') as f:
            print('some other content', file=f)
        module.publish(dest)
        assert check_dir(dest, {
            'file': b'from git',
            'new-file': b'some content',
            '.git': b'IGNORE',
        })

    def test_publish_missing_ref(self):
        """
        Attempt publish of version which doesn't exist.

        Should fail gracefully.
        """
        # TODO
