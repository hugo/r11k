import random
import os.path

from r11k.util import (
    download_file,
    fix_name,
    unfix_name,
)


def test_fix_name():
    assert 'name/unfixed-module' == fix_name('name-unfixed-module')


def test_unfix_name():
    assert 'name-unfixed-module' == unfix_name('name/unfixed-module')


def test_download_file(tmp_path, httpserver):
    data = random.randbytes(1000)
    dest = os.path.join(tmp_path, "outfile")
    httpserver.expect_request("/").respond_with_data(data)
    download_file(httpserver.url_for("/"), dest)
    with open(dest, 'rb') as f:
        assert data == f.read()
