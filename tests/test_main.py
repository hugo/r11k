import os.path
import logging
import json

import yaml
from typing import Any

import r11k.config
from r11k.puppet import PuppetMetadata, DependencySpec
import pygit2
from r11k.puppetfile import (
    PuppetFile,
    find_all_modules_for_environment,
    load_puppetfile,
    parse_puppetfile,
)
from tests.util.encoder import Encoder
from tests.util.git import DEFAULT_BRANCH

from r11k.puppetmodule import (
    PuppetModule,
)


def test_load_simple(tmp_path):
    """
    Check that basic loading of a puppetfile.yaml works.
    """
    puppetfile = {'modules': [
        {'name': 'puppetlabs-stdlib',
         'version': '8.1.0'},
        {'name': 'puppet-extlib',
         'version': '5.3.0'},
    ]}

    path = os.path.join(tmp_path, 'puppetfile.yaml')
    with open(path, 'w') as f:
        yaml.dump(puppetfile, stream=f)

    pf = load_puppetfile(path)
    assert pf
    assert isinstance(pf, PuppetFile)
    assert set(pf.modules.keys()) == set(x['name'] for x in puppetfile['modules'])


def test_load_with_import(tmp_path):
    """
    Check tha all modules gets merged on a simple include.
    """
    source = {'modules': [
        {'name': 'puppetlabs-stdlib',
         'version': '8.1.0'},
    ]}

    derivation = {
        'modules': [
            {'name': 'puppet-extlib',
             'version': '5.3.0'},
        ],
        'include': 'source.yaml'
    }

    source_path = os.path.join(tmp_path, 'source.yaml')
    derivation_path = os.path.join(tmp_path, 'derivation.yaml')

    with open(source_path, 'w') as f:
        yaml.dump(source, stream=f)

    with open(derivation_path, 'w') as f:
        yaml.dump(derivation, stream=f)

    pf = load_puppetfile(derivation_path)
    assert pf
    assert isinstance(pf, PuppetFile)
    assert set(pf.modules.keys()) == {'puppetlabs-stdlib', 'puppet-extlib'}


def test_import_which_overrides(tmp_path):
    """
    Check that the sources versions get priority over included.
    """

    source = {'modules': [
        {'name': 'puppetlabs-stdlib',
         'version': '8.1.0'},
    ]}

    derivation = {
        'modules': [
            {'name': 'puppetlabs-stdlib',
             'version': '9.0.0'},
        ],
        'include': 'source.yaml'
    }

    source_path = os.path.join(tmp_path, 'source.yaml')
    derivation_path = os.path.join(tmp_path, 'derivation.yaml')

    with open(source_path, 'w') as f:
        yaml.dump(source, stream=f)

    with open(derivation_path, 'w') as f:
        yaml.dump(derivation, stream=f)

    pf = load_puppetfile(derivation_path)
    assert pf
    assert pf.modules['puppetlabs-stdlib'].version == '9.0.0'


def test_find_all_modules(testconf: r11k.config.Config) -> None:
    modules = parse_puppetfile({
        'modules': [
            {
                'name': 'hugonikanor-B',
                'version': '1.0.0',
            },
        ]
    }, config=testconf)

    expected_modules = parse_puppetfile({
        'modules': [
            {
                'name': 'hugonikanor-B',
                'version': '1.0.0',
            },
            {
                'name': 'hugonikanor-C',
                'version': '1.0.0',
            },
        ]
    }, config=testconf)

    returns: dict[str, PuppetModule] = {}
    for module in find_all_modules_for_environment(modules).modules.values():
        returns[module.name] = module

    exp = list(expected_modules.modules.values())
    assert set(x.name for x in exp) == set(returns.keys())

    src_dict: dict[str, PuppetModule] = {x.name: x for x in exp}
    for name, module in returns.items():
        assert module.version == src_dict[name].version


def test_find_all_modules_recursive(testconf: r11k.config.Config,
                                    caplog: Any) -> None:
    caplog.set_level(logging.DEBUG)
    modules = parse_puppetfile({
        'modules': [
            {
                'name': 'hugonikanor-A',
                'version': '1.0.0',
            },
        ]
    }, config=testconf)

    expected_modules = parse_puppetfile({
        'modules': [
            {
                'name': 'hugonikanor-A',
                'version': '1.0.0',
            },
            {
                'name': 'hugonikanor-B',
                'version': '1.0.0',
            },
            {
                'name': 'hugonikanor-C',
                'version': '1.0.0',
            },
        ]
    }, config=testconf)

    returns: dict[str, PuppetModule] = {}
    for module in find_all_modules_for_environment(modules).modules.values():
        returns[module.name] = module

    exp = list(expected_modules.modules.values())
    assert set(x.name for x in exp) == set(returns.keys())

    src_dict: dict[str, PuppetModule] = {x.name: x for x in exp}
    for name, module in returns.items():
        assert module.version == src_dict[name].version


def test_git_dependencies(testconf, git_upstream_factory, tmp_path):
    """
    Module which depends on module with git source
    HugoNikanor-A → HugoNikanor-B
    At one point failed since module names wheren't properly kept track of
    """
    # Setup repos which we will clone later

    metadata = {
        'A': PuppetMetadata(name='HugoNikanor-A',
                            version='0.1.0',
                            author='Hugo Hörnquist',
                            license='UNKNOWN',
                            summary='Module which depends on module only available through git',
                            source='https://example.com/A',
                            dependencies=[DependencySpec(name='HugoNikanor/B',
                                                         version_requirement='>= 0.1.0 < 0.2.0')]),
        'B': PuppetMetadata(name='HugoNikanor-B',
                            version='0.1.0',
                            author='Hugo Hörnquist',
                            license='UNKNOWN',
                            summary='Dependency of A',
                            source='https://example.com/B',
                            dependencies=[]),
    }

    upstreams = {
        'A': git_upstream_factory('repo-A'),
        'B': git_upstream_factory('repo-B'),
    }

    for name in ['A', 'B']:
        workdir = os.path.join(tmp_path, 'workdir', f'repo-{name}')
        repo = pygit2.init_repository(workdir)
        with open(os.path.join(workdir, 'metadata.json'), 'w') as f:
            json.dump(metadata[name], f, indent=2, cls=Encoder)
        repo.index.add('metadata.json')
        repo.create_commit(f'refs/heads/{DEFAULT_BRANCH}',
                           pygit2.Signature('Sample Author', 'author@example.com'),
                           pygit2.Signature('Sample Commiter', 'commiter@example.com'),
                           'Add metadata.json',
                           repo.index.write_tree(),
                           [])
        origin = repo.remotes.create('origin', upstreams[name].path)
        origin.push([f'refs/heads/{DEFAULT_BRANCH}'])

    # Repo setup complete, start running r11k stuff

    puppetfile = parse_puppetfile({
        'modules': [
            {
                'name': 'A',
                'version': DEFAULT_BRANCH,
                'git': upstreams['A'].path,
            },
            {
                'name': 'B',
                'version': DEFAULT_BRANCH,
                'git': upstreams['B'].path,
            }
        ],
    }, config=testconf)

    modules = find_all_modules_for_environment(puppetfile).modules.values()
    assert {'HugoNikanor-A', 'HugoNikanor-B'} == set(module.name for module in modules)


def test_publish(tmp_path, testconf):
    """
    Test publish for a complete puppet file
    TODO write this test
    """
    # puppetfile = parse_puppetfile({
    # }, config=testconf)
