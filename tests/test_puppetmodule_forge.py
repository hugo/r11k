import os
import json
# import hashlib

from semver import VersionInfo

from r11k.puppet import PuppetMetadata

from r11k.puppetmodule.forge import (
    ForgePuppetModule,
    # get_versioned_forge_module_metadata,
)

from tests.util import check_dir


class TestForgeStuff:  # noqa: D101
    def test_metadata(self, testconf):  # noqa: D102
        module = ForgePuppetModule(name='hugonikanor-testlib',
                                   version='7.0.0',
                                   config=testconf)
        # Double check since .metadata caches the field, and we want
        # both branches to be covered
        assert isinstance(module.metadata, PuppetMetadata)
        assert isinstance(module.metadata, PuppetMetadata)

    def test_publish(self, testconf, tmp_path):  # noqa: D102

        expected_dirs = {
            '7.0.0': {
                'in-both.txt': b'This is in both versions',
                'metadata.json': json.dumps({
                    'name': 'hugonikanor-testlib',
                    'version': '7.0.0',
                    'author': 'hugonikanor',
                    'license': 'License text would have gone here',
                    'summary': 'Test module for R11K',
                    'source': 'https://example.com/hugonikanor/hugonikanor-testlib',
                    'dependencies': [],
                    'requirements': [],
                    'project_page': None,
                    'issues_url': None,
                    'operatingsystem_support': None,
                    'tags': None,
                }, indent=3).encode('UTF-8'),
                'only-in-lower.txt': b'This is only part of 7.0.0',
            },
            '8.0.0': {
                'in-both.txt': b'This is the updated version',
                'metadata.json': json.dumps({
                    'name': 'hugonikanor-testlib',
                    'version': '8.0.0',
                    'author': 'hugonikanor',
                    'license': 'License text would have gone here',
                    'summary': 'Test module for R11K',
                    'source': 'https://example.com/hugonikanor/hugonikanor-testlib',
                    'dependencies': [],
                    'requirements': [],
                    'project_page': None,
                    'issues_url': None,
                    'operatingsystem_support': None,
                    'tags': None,
                }, indent=3).encode('UTF-8'),
                'only-in-upper.txt': b'This is only part of 8.0.0',
            }
        }

        publish_dirs: dict[str, str] = {}

        for version in ['7.0.0', '8.0.0']:
            module = ForgePuppetModule(name='hugonikanor-testlib',
                                       version=version,
                                       config=testconf)
            path_base = os.path.join(tmp_path, 'modules', 'testlib')

            target = os.path.join(testconf.tar_cache, f'{module.name}-{module.version}.tar.gz')
            assert module.fetch() == target

            module.publish(path_base)
            # validate that the published version is correct
            with open(os.path.join(path_base, 'metadata.json')) as f:
                data = json.load(f)
                assert data['version'] == version

            module.publish(path_base)
            publish_dirs[version] = path_base
            check_dir(path_base, expected_dirs[version])

        # A re-publish after updating version should still be in the same directory.
        assert publish_dirs['7.0.0'] == publish_dirs['8.0.0']

    def test_versions(self, testconf):  # noqa: D102
        module = ForgePuppetModule('hugonikanor-testlib', config=testconf)
        versions = module.versions()
        assert versions == [VersionInfo.parse(x) for x in [
            '7.0.0',
            '8.0.0',
        ]]
        assert module.latest() == VersionInfo(8)


def test_publish_empty_tar():  # TODO
    pass


def test_publish_tar_with_non_dir_root():  # TODO
    pass


def test_publish_tar_dest_existst_non_dir():  # TODO
    pass
