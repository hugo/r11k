"""
R11K configuration and global "singletons".

Explicitly designed to be easy to substitute for another configuration
in tests.
"""

import os.path
from typing import (
    Optional
)

from xdg.BaseDirectory import xdg_cache_home

from r11k import util
from r11k.httpcache import HTTPCache


class Config:
    """
    Global configuration manager.

    :param cache_root: Where all cache directories should end up,
                       Defaults to `$XDG_CACHE_HOME/r11k`
    :param clone_base: Where cloned repos store (non-deployed repos)
                       should be stored, Defaults to `$cache_root/repos`.
    :param http_cache: Where misc. stuff downloaded ower HTTP should be
                       placed. Defaults to `$cache_root/http.`
    :param tar_cache: Where downloaded tarballs should be kept.
                      Defaults to `$cache_root/tar`
    :param http_ttl: How long until we retry downloading stuff over HTTP
    :param git_ttl: How long until we re-fetch git-repos.
    :param api_base: Base for all Puppet Forge calls. Defaults to
                     `https://forge.puppet.com`
    """

    def __init__(self,
                 cache_root: str = os.path.join(xdg_cache_home, 'r11k'),
                 clone_base: Optional[str] = None,
                 http_cache: Optional[str] = None,
                 tar_cache: Optional[str] = None,
                 http_ttl: int = 3600,
                 git_ttl: int = 3600,
                 api_base: str = 'https://forge.puppet.com'):

        self.cache_root = cache_root
        self.clone_base: str = clone_base or os.path.join(cache_root, 'repos')
        self.http_cache: str = http_cache or os.path.join(cache_root, 'http')
        self.tar_cache: str = tar_cache or os.path.join(cache_root, 'tar')
        self._http_ttl = http_ttl
        self.git_ttl = git_ttl
        self.api_base = api_base

        cache_dirs = [
            self.cache_root,
            self.clone_base,
            self.http_cache,
            self.tar_cache,
        ]
        for dir in cache_dirs:
            util.ensure_directory(dir)

        self.httpcache: HTTPCache = HTTPCache(self.http_cache, default_ttl=self._http_ttl)

    @property
    def http_ttl(self) -> int:
        """Return http_ttl field."""
        return self._http_ttl

    @http_ttl.setter
    def http_ttl(self, value: int) -> None:
        """Save a new TTL value, and forwards it to `self.httpcache.default_ttl`."""
        self._http_ttl = value
        self.httpcache.default_ttl = value


config = Config()
"""Default configuration instance when none has been defined."""
