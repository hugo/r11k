"""
Python mapping of Puppet's metadata files.

Each Puppet module *ought* to contain a `metadata.json` file, and
*all* modules on the Puppet Forge has a metadata file, including ways
to retrieve only them through the Forge's API.

Puppets metadata format is [documented here][metadata], and is mapped
almost directly here. Worth noting that items are mapped to python
types.

All constructors take any extra keyword arguments to future proof
against future versions of Puppets metadata files.

[metadata]: https://puppet.com/docs/puppet/7/modules_metadata.html
"""

from typing import (
    Any,
    Optional,
    Sequence,
    Union,
)

from .interval import Interval
from .util import unfix_name
from .version import parse_version


class DependencySpec:  # pragma: no cover
    """
    A single dependency of a puppet module.

    :param name: is (probly) a the Forge name
    :param version_requirement: is a string on the form `>= 4.1.0 < 8.0.0"
    """

    def __init__(self, name: str, version_requirement: str):
        self.name = unfix_name(name)
        self.version_requirement = version_requirement

    def interval(self, by: str) -> Interval:
        """Return maching interval object."""
        cmp_a, min, cmp_b, max = parse_version(self.version_requirement)
        return Interval(min, max, cmp_a, cmp_b, by=by)

    def __repr__(self) -> str:
        return f'DependencySpec({repr(self.name)}, {repr(self.version_requirement)})'


class OSSupport:
    """Operatingsystem a puppet module supports."""

    def __init__(self,
                 operatingsystem: str,
                 operatingsystemrelease: Optional[list[str]] = None,
                 **_: Any):  # pragma: no cover
        self.operatingsystem = operatingsystem
        self.operatingsystemrelease = operatingsystemrelease


class PuppetMetadata:
    """
    Metadata of a puppet module.

    The parameters which are unions between a simple Python type, and
    a fully realized type *will* get parsed into the fully realized
    type in the constructor.
    """

    def __init__(self,
                 name: str,
                 version: str,
                 author: str,
                 license: str,
                 summary: str,
                 source: str,
                 dependencies: Sequence[Union[dict, DependencySpec]],
                 requirements: list[Union[dict, DependencySpec]] = [],
                 project_page: Optional[str] = None,
                 issues_url: Optional[str] = None,
                 operatingsystem_support: Optional[list[Union[dict, OSSupport]]] = None,
                 tags: Optional[list[str]] = None,
                 **_: Any):  # pragma: no cover
        self.name: str = name
        """
        Concatenation of owner's Forge username with the modules
        name, on the form *forge username*-*module name*.
        """
        self.version: str = version
        """Semver formatted string of version."""
        self.author: str = author
        """Arbitrarly formatted name of author."""
        self.license: str = license
        """Software License of module.

        *Should* be present in [SPDX's list of licenses](https://spdx.org/licenses/)
        """
        self.summary: str = summary
        """One-line summary of module."""
        self.source: str = source
        """URL to source code of module."""
        self.dependencies: list[DependencySpec] \
            = [d if isinstance(d, DependencySpec) else DependencySpec(**d)
               for d in dependencies]
        """List of other Puppet modules this module dependes on."""
        self.requirements: list[DependencySpec] \
            = [d if isinstance(d, DependencySpec) else DependencySpec(**d)
               for d in requirements]
        """List of external requirements.

        This includes Puppet version."""
        self.project_page: Optional[str] = project_page
        """URL to project page of module."""
        self.issues_url: Optional[str] = issues_url
        """URL to where issues can be submitted."""
        self.operatingsystem_support: Optional[list[OSSupport]]
        """List of which operating systems this module is "compatible" with."""
        if operatingsystem_support:
            self.operatingsystem_support \
                = [x if isinstance(x, OSSupport) else OSSupport(**x)
                   for x in operatingsystem_support]
        else:
            self.operatingsystem_support = None
        self.tags: Optional[list[str]] = tags
        """Keywords for module discovery."""
