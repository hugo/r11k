"""Interactive entry point."""

from r11k.main import __main
import sys

if __name__ == '__main__':
    sys.exit(__main())
