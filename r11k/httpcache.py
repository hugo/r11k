"""Extension of `r11k.cache.KeyValueStore`. Should possibly be merged."""

import os
import os.path
from datetime import datetime
import time
import requests
from pathlib import Path

from typing import Any, Optional

import r11k
from r11k.cache import KeyValueStore


class HTTPCache:
    """
    HTTP client with built in cache.

    First caches any (successful) request for one hour (configurable),
    after the local TTL has expired the request is re-fired, but with
    a 'If-Modified-Since' header set.

    :param path: Where in the filesystem cached files are stored.
    :param default_ttl: How long (in seconds) we should cache files before re-fetching.
    """

    def __init__(self, path: str = '/tmp', default_ttl: int = 3600):
        self.store = KeyValueStore(path)
        self.default_ttl = default_ttl

    def get(self, url: str) -> Optional[Any]:
        """
        Fetch (from internet or cache) the contents of url.

        :return: A json blob
        """
        key = url
        if existing := self.store.get(key):
            path = self.store.path(key)
            try:
                st = os.stat(path)
                last_modified = datetime.utcfromtimestamp(st.st_mtime)
                now = datetime.utcfromtimestamp(time.time())
                if abs(now - last_modified).seconds < self.default_ttl:
                    return existing
            except FileNotFoundError:
                return existing

            response = self.__fetch(url, last_modified)
            if response.status_code == 304:
                Path(path).touch()
                return existing
            elif response.status_code != 200:
                raise RuntimeError('Invalid response code', response.status_code, url)

            data = response.json()
            self.store.put(key, data)
            return data

        else:
            response = self.__fetch(url)
            if response.status_code != 200:
                raise RuntimeError('Invalid response code', response.status_code, url)
            data = response.json()
            self.store.put(key, data)
            return data

    def __fetch(self, url: str, last_modified: Optional[datetime] = None) -> requests.Response:
        headers = {'User-Agent': f'r11k/{r11k.VERSION}'}
        if last_modified:
            date = last_modified.strftime('%a, %d %b %Y %H:%M:%S GMT')
            headers['If-Modified-Since'] = date
        return requests.get(url, headers=headers)
