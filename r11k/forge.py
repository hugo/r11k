"""
Python instances of all objects returned by the Puppet Forge API.

This code could have been generated from their OpenAPI specification,
but the Python tools for it doesn't feel mature enough. Due to that
the names below are a bit arbitrary.

Full documentation about each field can be found at
https://forgeapi.puppet.com/
"""

from typing import (
    Any,
    Literal,
    Optional,
    TypedDict,
    Union,
)
from datetime import datetime
from semver import VersionInfo
from r11k.puppet import PuppetMetadata


def _parse_date(date: str) -> datetime:
    """Parse date from the format puppet forge uses (almost ISO8601)."""
    return datetime.strptime(date, "%Y-%m-%d %H:%M:%S %z")


class Superseeded(TypedDict):  # pragma: no cover
    """Info about which modulesuperseeded the current module."""

    uri: str
    slug: str


class Owner(TypedDict):  # pragma: no cover
    """Owner of a given puppet module."""

    uri: str
    slug: str
    username: str
    gravatar_id: str


class Release:
    """
    Release info about a puppet module.

    Returned by `/v3/releases` (the list and search endpoint).
    """

    def __init__(self,
                 uri: str,
                 slug: str,
                 version: Union[str, VersionInfo],
                 created_at: Union[str, datetime],
                 deleted_at: Optional[Union[str, datetime]],
                 file_uri: str,
                 file_size: int,
                 **_: Any):  # pragma: no cover
        self.uri = uri
        self.slug = slug
        self.version: VersionInfo

        if isinstance(version, VersionInfo):
            self.version = version
        else:
            self.version = VersionInfo.parse(version)

        self.file_uri = file_uri
        self.file_size = file_size

        self.created_at: datetime
        if isinstance(created_at, datetime):
            self.created_at = created_at
        else:
            self.created_at = _parse_date(created_at)

        self.deleted_at: Optional[datetime] = None
        if isinstance(deleted_at, datetime):
            self.deleted_at = deleted_at
        elif isinstance(deleted_at, str):
            self.deleted_at = _parse_date(deleted_at)


class ForgeModule:
    """
    Data about a puppet module.

    Return value of `/v3/modules` (the list and search endpoint).
    """

    def __init__(self,
                 uri: str,
                 slug: str,
                 name: str,
                 deprecated_at: Optional[Union[str, datetime]],
                 owner: Owner,
                 **_: Any):  # pragma: no cover
        self.uri = uri
        self.slug = slug
        self.name = name
        self.deprecated_at: Optional[datetime] = None
        if isinstance(deprecated_at, datetime):
            self.deprecated_at = deprecated_at
        elif isinstance(deprecated_at, str):
            self.deprecated_at = _parse_date(deprecated_at)
        self.owner = owner


class CurrentRelease(Release):
    """
    Extended release info about a puppet module.

    Superset of Releases for the currently active version (or an older
    if explicitly specified).
    """

    def __init__(self,
                 uri: str,
                 slug: str,
                 module: Union[ForgeModule, dict],
                 version: Union[str, VersionInfo],
                 metadata: Union[PuppetMetadata, dict],
                 tags: list[str],
                 pdk: bool,
                 validation_score: int,
                 file_uri: str,
                 file_size: int,
                 file_md5: str,
                 file_sha256: str,
                 downloads: int,
                 readme: str,
                 changelog: str,
                 license: str,
                 reference: str,
                 tasks: list[dict],
                 plans: list[dict],
                 created_at: Union[str, datetime],
                 updated_at: Union[str, datetime],
                 deleted_at: Optional[Union[str, datetime]],
                 deleted_for: Optional[str],
                 pe_compatibility: Optional[list[str]] = None,
                 **_: Any):  # pragma: no cover
        super().__init__(uri, slug, version, created_at, deleted_at,
                         file_uri, file_size)
        self.module: ForgeModule
        if isinstance(module, ForgeModule):
            self.module = module
        else:
            self.module = ForgeModule(**module)

        self.metadata: PuppetMetadata
        if isinstance(metadata, PuppetMetadata):
            self.metadata = metadata
        else:
            self.metadata = PuppetMetadata(**metadata)

        self.tags = tags
        self.pdk = pdk
        self.validation_score = validation_score
        self.file_md5 = file_md5
        self.file_sha256 = file_sha256
        self.downloads = downloads
        self.readme = readme
        self.changelog = changelog
        self.license = license
        self.reference = reference
        self.pe_compatibility = pe_compatibility
        self.tasks = tasks
        self.plans = plans

        self.updated_at: datetime
        if isinstance(updated_at, datetime):
            self.updated_at = updated_at
        else:
            self.updated_at = _parse_date(updated_at)
        self.deleted_for = deleted_for


class FullForgeModule(ForgeModule):
    """
    Extended data about a puppet module.

    The extended superset when only checking a single module.

    Return value of `/v3/modules/{module_slug}`.
    """

    def __init__(self,
                 uri: str,
                 slug: str,
                 name: str,
                 downloads: int,
                 created_at: Union[str, datetime],
                 updated_at: Union[str, datetime],
                 deprecated_at: Optional[Union[str, datetime]],
                 deprecated_for: Optional[str],
                 superseded_by: Optional[Superseeded],
                 endorsement: Optional[Union[Literal['supported'],
                                             Literal['approved'],
                                             Literal['partner']]],
                 module_group: Union[Literal['base'], Literal['pe_only']],
                 premium: bool,
                 owner: Owner,
                 current_release: Union[CurrentRelease, dict],
                 releases: list[Union[Release, dict]],
                 feedback_score: int,
                 homepage_url: str,
                 issues_url: str,
                 **_: Any):  # pragma: no cover
        super().__init__(uri, slug, name, deprecated_at, owner)
        self.downloads = downloads

        self.created_at: datetime
        if isinstance(created_at, datetime):
            self.created_at = created_at
        else:
            self.created_at = _parse_date(created_at)

        self.update_at: datetime
        if isinstance(updated_at, datetime):
            self.updated_at = updated_at
        else:
            self.updated_at = _parse_date(updated_at)

        self.deprecated_for = deprecated_for
        self.superseded_by = superseded_by
        self.endorsement = endorsement
        self.module_group = module_group
        self.premium = premium
        self.owner = owner

        self.current_release: CurrentRelease
        if isinstance(current_release, CurrentRelease):
            self.current_release = current_release
        else:
            self.current_release = CurrentRelease(**current_release)

        self.releases: list[Release]
        self.releases = [x if isinstance(x, Release) else Release(**x) for x in releases]

        self.feedback_score = feedback_score
        self.homepage_url = homepage_url
        self.issues_url = issues_url
