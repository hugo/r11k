"""Simple Key-Value cache backed by disk."""

import os

import json
import atexit
import hashlib
from r11k import util

from typing import Any, Optional


def _encode(string: str) -> str:
    """Calculate cheksum string for string."""
    return hashlib.sha256(string.encode('UTF-8')).hexdigest()


class KeyValueStore:
    """
    A simple key-value store.

    Serializes its data to disk as one json file for each key.
    Serialization to disk happens upon program end, through an
    `atexit` handler.

    ### Example

    If the data is
    >>> { 'a': 1, 'b': { 'c': 3 } }

    then the data is stored as:

    - `<path>`
      - `a.json`, *content*: `1`
      - `b.json`, *content*: `{"c":3}`
    """

    def __init__(self, path: str = '/tmp'):
        self._path: str = path
        self.data: dict[str, Any] = {}
        self._index: dict[str, str] = {}

        util.ensure_directory(path)

        atexit.register(self.save)

    def save(self) -> None:
        """Serialize ourselves to disk."""
        for key, value in self.data.items():
            filename = os.path.join(self._path, key)
            with open(filename, 'w') as f:
                json.dump(value, f)

        index_file = os.path.join(self._path, 'index.json')
        # Load existing index
        try:
            with open(index_file) as f:
                index = json.load(f)
        except FileNotFoundError:
            index = {}
        # Merge with current index
        index |= self._index

        # Write new index
        with open(index_file, 'w') as f:
            json.dump(index, f)

    def path(self, key: str) -> str:
        """Return filesystem path where this key would be stored."""
        key = _encode(key)
        return os.path.join(self._path, key)

    def get(self, key: str) -> Optional[Any]:
        """
        Get the value for key, possibly loading it from disk.

        Lazily loads it from disk. Return None if not found.
        """
        key = _encode(key)
        if it := self.data.get(key):
            return it
        else:
            filename = os.path.join(self._path, key)
            if os.path.exists(filename):
                with open(filename) as f:
                    data = json.load(f)
                    self.data[key] = data
                    return data
            else:
                return None

    def put(self, key: str, value: Any) -> None:
        """Store the given value under key."""
        _key = _encode(key)
        self.data[_key] = value
        self._index[key] = _key
