"""Minor utilities which didn't fit anywhere else."""

import logging
import os
import requests


logger = logging.getLogger(__name__)


def ensure_directory(path: str) -> None:  # pragma: no cover
    """Mkdir, but don't fail on existing directory."""
    try:
        os.mkdir(path)
    except FileExistsError:
        pass


def download_file(url: str, outfile: str) -> None:
    """Fetch file from url, save it to outfile."""
    logger.info('Downloading %s', outfile)
    r = requests.get(url, stream=True)
    if r.status_code != requests.codes.ok:
        r.raise_for_status()
    with open(outfile, 'wb') as fd:
        for chunk in r.iter_content(chunk_size=128):
            fd.write(chunk)


def unfix_name(name: str) -> str:
    """Transform <namespace>/<name> into <namespace>-<name>."""
    return '-'.join(name.split('/', 1))


def fix_name(name: str) -> str:
    """Transform <namespace>-<name> into <namespace>/<name>."""
    return '/'.join(name.split('-', 1))
