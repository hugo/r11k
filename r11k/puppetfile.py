"""
Definition and operations on our puppetfile format.

This file includes:
- The specification of our puppetfile.yaml format
  - The thing which publishes our environments
- the dependency resolver

## Puppetfile.yaml

A top level key `modules`, containing a list where each element is a dictionary
containing

:param name: Either the forge module name, or the target name for a git repo
:param [version]: Which version to use.
:param [git]: Git repo to clone to retrieve this object. Mutually exclusive with `http`
:param [http]: URL where a tarball can be found. Mutually exclusive with `git`

If neither a git nor http key is given then it's fetched from the
[Puppet Forge][forge]

#### `version`
If it's a Forge module then the version is looked up in the Forge. If it's a Git
repo then the version indicates a given ref (tag, branch, commit, ...). For HTTP
the version field is ignored.

An absent version field means latest compatible.

#### `http`
Is currently not implemented.

### Sample Puppetfile

```yaml
modules:
  - name: stdlib
    version: 8.2.0
  - name: extlib
  - name: mymodule
    git: https://git.example.com/a-different-repo-name.git
```

--------------------------------------------------

[forge]: https://forge.puppet.com
"""

import logging
import os.path

from dataclasses import dataclass, field
from functools import reduce
from typing import (
    Any,
    Optional,
)

import yaml

import r11k.config
from r11k import util
from r11k.interval import Interval
from r11k import interval
from r11k.puppetmodule import (
    PuppetModule,
    ForgePuppetModule,
    parse_module_dict,
)


logger = logging.getLogger(__name__)


@dataclass
class Hiera:
    """
    Contents of a hiera.yaml.

    https://puppet.com/docs/puppet/7/hiera_config_yaml_5.html
    """

    version: int = 5
    hierarchy: list[dict[str, str]] = field(default_factory=list)
    defaults: Optional[dict[str, str]] = None
    default_hierarchy: Optional[list[dict[str, str]]] = None


@dataclass
class PuppetFile:
    """Complete contents of a puppetfile.yaml."""

    # TODO do I really want a dict here? Would a set be better?
    modules: dict[str, PuppetModule] = field(default_factory=dict)
    """
    Module declarations from puppet file. Mapping from module name to
    module.
    """
    environment_name: Optional[str] = None
    """Name of the environment, used when publishing"""
    data: dict[str, dict[str, Any]] = field(default_factory=dict)
    """
    Hiera data which will be published as part of the environment.
    Each top level key is a path (with slashes for delimiters), but
    the .yaml suffix omitted. The dictionaries under that will be
    written to the output file.
    """
    hiera: Optional[Hiera] = None
    """hiera.yaml for this environment."""
    config: r11k.config.Config = field(default_factory=lambda: r11k.config.config)
    """r11k configuration"""

    def include(self, parent: 'PuppetFile') -> None:
        """
        Include a parent to this PuppetFile.

        ### Merging Strategies
        #### modules
        Each module from the parent is included, when both specify the
        same plugin, the version specified by us gets chosen.
        #### hiera
        We keep ours if we have one, otherwise we take it from the
        parent.
        #### data
        The set of files from both instances will be merged,
        defaulting to our version. The keys inside a file aren't
        merged.
        """
        self.modules = parent.modules | self.modules

        if not self.hiera:
            self.hiera = parent.hiera

        self.data = parent.data | self.data

    def serialize(self) -> dict[str, Any]:
        """Serialize back into format of puppetfile.yaml."""
        # data omitted since that gets published as data directory
        # hiera is ommitted since its published as hiera.yaml
        # environment name ommited since that becomes the target directory
        return {'modules': {k: v.serialize() for k, v in self.modules.items()}}


@dataclass
class ResolvedPuppetFile(PuppetFile):
    """
    Identical to PuppetFile, but modules is the true set.

    PuppetFile contains the user supplied modules from the
    puppetfile.yaml, this instead has the true set of modules, where
    all modules are present, and ALL of them will have exact versions
    (when needed).

    It's constructor should be seen assumed private. These objects
    should only be built through find_all_modules_for_environment.
    """

    def publish(self, destination: str) -> None:
        """
        Publish actual environments to direcotry.

        [Parameters]
            destination - Where to publish to.
                          This is most likely
                          /etc/puppetlabs/code/environments/{env_name}
        """
        logger.info('== Building actual environment ==')
        util.ensure_directory(destination)
        for module in self.modules.values():
            logger.debug('Including %s', module)
            path = os.path.join(destination, 'modules', module.module_path)
            module.publish(path)

        if self.hiera:
            with open(os.path.join(destination, 'hiera.yaml'), 'w') as f:
                yaml.dump(self.hiera, f)

        if self.data:
            for path, data in self.data.items():
                # Re-normalize path for systems with other path delimiters
                path = os.path.join(*path.split('/'))
                path = os.path.join(destination, 'data', f'{path}.yaml')
                os.makedirs(os.path.dirname(path), exist_ok=True)
                with open(path, 'w') as f:
                    yaml.dump(data, f)

        with open(os.path.join(destination, 'puppetfile.yaml'), 'w') as f:
            yaml.dump(self.serialize(), f)


def parse_puppetfile(data: dict[str, Any],
                     *,
                     config: Optional[r11k.config.Config] = None,
                     path: Optional[str] = None) -> PuppetFile:
    """Parse data from puppetfile dictionary."""
    pf = PuppetFile()

    if subpath := data.get('include'):
        if not path:
            raise ValueError('include only possible when we have a source file')
        inc_path = os.path.join(os.path.dirname(path), subpath)
        parent = load_puppetfile(inc_path)
        pf.include(parent)

    if config:
        pf.config = config

    removed: list[str] = []
    for module in data['modules']:
        if config:
            module['config'] = config
        m = parse_module_dict(module)
        if m == 'remove':
            removed.append(module['name'])
        else:
            m.explicit = True
            pf.modules[m.name] = m

    for r in removed:
        # This WILL fail if we try to remove a non-added module.
        # Possibly catch this and instead throw a better error message
        # that this is by design.
        del pf.modules[r]

    if env := data.get('environment'):
        pf.environment_name = env
    elif path:
        pf.environment_name, _ = os.path.splitext(os.path.basename(path))

    if data_entry := data.get('data'):
        pf.data = data_entry

    if hiera := data.get('hiera'):
        pf.hiera = hiera

    return pf


def load_puppetfile(filepath: str) -> PuppetFile:
    """Load puppetfile.yaml."""
    logger.debug('Loading %s', filepath)
    with open(filepath) as f:
        data = yaml.full_load(f)
        return parse_puppetfile(data, path=filepath)


def update_module_names(modules: list[PuppetModule]) -> None:
    """
    Update module name from metadata.

    Compare the currently known module name (which probably orginated
    in the puppetfile) with the name in the modules metadata. Update
    to the name from the metadata if they differ.
    """
    logger.info('== Updating modules (and extracting dependencies) ==')
    # for every explicit module
    for module in modules:
        metadata = module.metadata
        # Here since we want the "true" name (as per the metadata)
        if module.name != metadata.name:
            logger.info('Module and metadata names differ, updating module to match metadata.')
            logger.info(f'{module.name} ≠ {metadata.name}')
            module.name = metadata.name


# TODO figure out proper way to propagate config through everything
# TODO shouldn't this be a method on PuppetFile?
def find_all_modules_for_environment(puppetfile: PuppetFile) -> ResolvedPuppetFile:
    """
    Find all modules which would make out this puppet environment.

    Returns a list of modules, including which version they should be.
    """
    modules: list[PuppetModule] = list(puppetfile.modules.values())

    update_module_names(modules)

    known_modules: dict[str, PuppetModule] = {}
    for module in modules:
        known_modules[module.name] = module

    # Resolve all modules, restarting with the new set of modules
    # repeatedly until we find a stable point.
    while True:
        # Dict from which modules we want, to which versions we can have
        constraints: dict[str, list[Interval]] = {}

        # For each module in puppetfile
        for module in modules:
            logger.debug('Handling %s', module)
            # collect its dependencies
            for depspec in module.metadata.dependencies:
                # and merge them to the dependency set
                constraints.setdefault(depspec.name, []) \
                           .append(depspec.interval(by=module.name))

        # resolve constraints
        resolved_constraints: dict[str, Interval] = {}
        for module_name, intervals in constraints.items():
            # TODO what to do if invalid constraints
            resolved_constraints[module_name] = reduce(interval.intersect, intervals)

        # build the next iteration of modules
        # If this turns out to be identical to modules, then
        # everything is resolved and we exit. Otherwise we continue
        # the loop
        next_modules: dict[str, PuppetModule] = {}
        for name, interval_ in resolved_constraints.items():
            if module_ := known_modules.get(name):
                next_modules[name] = module_
            else:
                # TODO keep interval instead of locking it in
                module_ = ForgePuppetModule(name, config=puppetfile.config)
                # TODO this crashes when the dependency is a git
                # module, since it tries to fetch a forge module of
                # that name.
                # TODO continue here, fix this
                module_.version = interval_.newest(module_.versions())
                next_modules[name] = module_
                known_modules[name] = module_

        # TODO what are we even forcing here?
        for name, module_ in puppetfile.modules.items():
            if next_modules.get(name):
                logger.warn('Forcing %s', name)
            next_modules[name] = module_

        next_modules_list: list[PuppetModule] = list(next_modules.values())

        if next_modules_list == modules:
            break

        modules = next_modules_list

    return ResolvedPuppetFile(modules={m.name: m for m in modules},
                              environment_name=puppetfile.environment_name,
                              data=puppetfile.data,
                              hiera=puppetfile.hiera,
                              config=puppetfile.config)
