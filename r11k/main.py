"""Entry point for r11k."""

import argparse
import locale
import logging
import os
import os.path
import graphviz

from r11k import util
from r11k.puppetfile import (
    load_puppetfile,
    find_all_modules_for_environment,
)
import r11k.config


logger = logging.getLogger(__name__)

# TODO warn if not latest


def __main() -> int:

    locale.resetlocale()

    parser = argparse.ArgumentParser(description='r10k, but good')
    parser.add_argument('puppetfile', help='Puppetfile, but in yaml format')
    parser.add_argument('--dest', help='Directory to deploy environment to',
                        dest='dest', default='/etc/puppetlabs/code/environments/')
    parser.add_argument('--env-name', help='Force name of environment', dest='environment_name')
    parser.add_argument('--force', help='Refresh all caches', dest='force',
                        action='store_true')
    parser.add_argument('--log', help='Set output verbosity',
                        dest='loglevel', action='store')

    args = parser.parse_args()

    if args.force:
        r11k.config.config.http_ttl = 0
        r11k.config.config.git_ttl = 0

    root_logger = logging.getLogger('')
    root_logger.setLevel(logging.WARNING)
    if args.loglevel:
        root_logger.setLevel(args.loglevel)

    ch = logging.StreamHandler()

    formatter: logging.Formatter
    try:
        import colorlog
        formatter = colorlog.ColoredFormatter('%(log_color)s%(levelname)s:%(name)s:%(message)s')
    except ModuleNotFoundError:
        formatter = logging.Formatter('%(name)s %(lineno)i - %(levelname)s - %(message)s')

    ch.setFormatter(formatter)
    root_logger.addHandler(ch)

    root_logger.debug('Logging initialized')

    puppetfile = load_puppetfile(args.puppetfile)

    resolvedPuppetfile = find_all_modules_for_environment(puppetfile)

    # ----- Publish to destination ---------------------

    destdir = args.dest
    util.ensure_directory(destdir)

    if args.environment_name:
        resolvedPuppetfile.environment_name = args.environment_name

    if not resolvedPuppetfile.environment_name:
        raise ValueError()

    dest = os.path.join(destdir, resolvedPuppetfile.environment_name)
    root_logger.debug('Publishing to %s', dest)
    resolvedPuppetfile.publish(dest)

    # ----- Build Graphviz file ------------------------

    dot = graphviz.Digraph()
    dot.attr(rankdir='LR')
    # print('graph [layout=dot rankdir=LR]', file=f)
    for module in resolvedPuppetfile.modules.values():
        # node_id = hashlib.md5(module.name.encode('UTF-8')).hexdigest()
        # if module.name in puppetfile.modules:
        if module.explicit:
            dot.node(module.name, fillcolor='lightgreen', style='filled')
        else:
            dot.node(module.name)
        # print(f'm{node_id} [label="{module.name}"]', file=f)
        for dependency in module.metadata.dependencies:
            # other_id = hashlib.md5(dependency.name.encode('UTF-8')).hexdigest()
            # print(f'm{node_id} -> m{other_id} [label="{dependency.version_requirement}"]', file=f)
            dot.edge(module.name, dependency.name, label=dependency.version_requirement)

    dot.render('graph.dot')

    return 0

# possible mains:
# install
#   installs everything from the given resolution file
# update
#   updates (without checking) everything from the current resolution file
# check
#   checks the given resolution file, and reports all possible errors
# check-dist <dist>
#   checks if the given resolution file is valid for the current
#   distribution
# check-update
#   check how an update of everything would resolve the dependency tree
#   / alternativly, check how much stuff can be updated without
#     breaking the dependency tree
