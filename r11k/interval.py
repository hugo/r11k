"""
Intervals of versions.

Also includes operations for manipulating and comparing those
intervals.
"""

import operator

from typing import (
    Optional,
)

from semver import VersionInfo

from .version import (
    Comp,
    max_version,
    min_version,
)


class Interval:
    """
    An interval of versions.

    Keeps information about the endpoints, along with who imposed that
    constraint.

    ### Examples and operators
    Two intervals are equal (with regards to `==`) only if they are
    identical, including their `max_by` and `min_by` fields.

    Intervals supports the `in` operator for `VersionInfo` objects,
    returning if the given version is within the intervals bounds.

    :param minimi: Lower bound of the interval, default to 0.0.0
    :param maximi: Upper bound of the interval, defaults to (2⁶³).0.0
                         (which is basically infinity)
    :param min_cmp:
    :param max_cmp:
    :param by: Fallback for both `min_by` and `max_by` if either isn't given.
    :param min_by:
    :param max_by:

    :raises: `ValueError` if `self.minimi > self.maximi`.
    """

    def __init__(self,
                 minimi: VersionInfo = min_version,
                 maximi: VersionInfo = max_version,
                 min_cmp: Comp = operator.gt,
                 max_cmp: Comp = operator.le,
                 *,
                 by: Optional[str] = None,
                 min_by: Optional[str] = None,
                 max_by: Optional[str] = None,
                 ):

        if min_cmp not in [operator.gt, operator.ge]:
            raise ValueError("Only '>' and '>=' are valid for min_cmp")
        if max_cmp not in [operator.lt, operator.le]:
            raise ValueError("Only '<' and '<=' are valid for max_cmp")

        self.minimi: VersionInfo = minimi
        """Lower bound for interval."""
        self.maximi: VersionInfo = maximi
        """Upper bound for interval."""
        self.min_cmp: Comp = min_cmp
        """Comparison operator for the lower bound, *must* be `>` or `≥`."""
        self.max_cmp: Comp = max_cmp
        """Comparison operator for the upper bound, *must* be `<` or `≤`."""

        self.min_by: str = min_by or by or ""
        """
        A description of who imposed the lower constraint in this interval.

        Used to later inspect why this interval looks like it does.
        Retrival of this field should only be for display to the user,
        and no assumptions should be made about the format. It will
        however probably either be the name of a Puppet module, or a
        comma separated list of them.
        """
        self.max_by: str = max_by or by or ""
        """Like `min_by`, but for the upper bound."""

        if self.minimi > self.maximi:
            raise ValueError("The end of an interval must be greater (or equal) to the start")

    def newest(self, lst: list[VersionInfo]) -> VersionInfo:
        """Find the largest version from list which is inside this interval."""
        for v in sorted(lst, reverse=True):
            if v in self:
                return v
        raise ValueError('No matching versions')

    def __contains__(self, v: VersionInfo) -> bool:
        return bool(self.min_cmp(v, self.minimi) and self.max_cmp(v, self.maximi))

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Interval):
            return False

        return all([self.minimi == other.minimi,
                    self.maximi == other.maximi,
                    self.min_cmp == other.min_cmp,
                    self.max_cmp == other.max_cmp,
                    self.min_by == other.min_by,
                    self.max_by == other.max_by])

    def __repr__(self) -> str:
        op = {operator.gt: "operator.gt",
              operator.ge: "operator.ge",
              operator.lt: "operator.lt",
              operator.le: "operator.le"}
        args = ', '.join([repr(self.minimi),
                          repr(self.maximi),
                          op.get(self.min_cmp, str(self.min_cmp)),
                          op.get(self.max_cmp, str(self.min_cmp)),
                          f'min_by={repr(self.max_by)}',
                          f'max_by={repr(self.max_by)}'])
        return f'Interval({args})'

    def __str__(self) -> str:
        return ''.join([
            '[' if self.min_cmp == operator.ge else '(',
            str(self.minimi),
            ', ',
            str(self.maximi),
            ']' if self.max_cmp == operator.le else ')',
            ' ',
            f'upper={repr(self.max_by)}, ',
            f'lower={repr(self.min_by)}',
        ])


def __join_opt_strings(*strings: Optional[str]) -> Optional[str]:
    """Join all non-None strings."""
    out: list[str] = []
    for s in strings:
        if s:
            out.append(s)
    if out:
        return ','.join(out)
    else:
        return None


def overlaps(a: Interval, b: Interval) -> bool:
    """
    Check if interval a overlaps interval b.

    The case where `a.maximi == b.minimi` (or vice versa) only
    overlaps when both intervals are inclusive in that point.
    """
    # Case 1      Case 2       Case 3        Case 4
    # [---)       [---]        [---)         [---]
    #     (---]       (---]        [---]         [---]
    if a.maximi == b.minimi:
        return bool(a.max_cmp == operator.le
                    and b.min_cmp == operator.ge)
    elif a.minimi == b.maximi:
        return bool(a.min_cmp == operator.ge
                    and b.max_cmp == operator.le)
    # Case 9
    # [---] [---]
    elif min(a.maximi, b.maximi) < max(a.minimi, b.minimi):
        return False

    # Case 5       Case 8
    # [----]           [--]
    #    [---]     [---------]
    elif a.min_cmp(b.minimi, a.minimi) and a.max_cmp(b.minimi, a.maximi):
        return True

    # Case 6         Case 7
    #     [----]     [--------]
    # [-----]           [--]
    elif b.min_cmp(a.minimi, b.minimi) and b.max_cmp(a.minimi, b.maximi):
        return True

    else:
        return False


def intersect(a: Interval, b: Interval) -> Interval:
    """
    Return the intersection of the two given intervals.

    :raises: `ValueError` if the two intervals don't overlap.
    """
    minimi: VersionInfo
    maximi: VersionInfo
    min_by: Optional[str]
    max_by: Optional[str]
    min_cmp: Comp
    max_cmp: Comp

    if not overlaps(a, b):
        raise ValueError("Only overlapping intervals can intersect")

    # Lower Bound
    if a.minimi == b.minimi:
        minimi = a.minimi
        if a.min_cmp == b.min_cmp:
            # pick either one
            min_cmp = a.min_cmp
            min_by = __join_opt_strings(a.min_by, b.min_by)
        elif a.min_cmp == operator.ge and b.min_cmp == operator.gt:
            min_cmp = b.min_cmp
            min_by = b.min_by
        elif b.min_cmp == operator.ge and a.min_cmp == operator.gt:
            min_cmp = a.min_cmp
            min_by = a.min_by
        else:
            raise ValueError("Invalid comperators")

    elif a.minimi < b.minimi:
        minimi = a.minimi
        min_by = a.min_by
        min_cmp = a.min_cmp

    elif a.minimi > b.minimi:
        minimi = b.minimi
        min_by = b.min_by
        min_cmp = b.min_cmp

    # Upper Bound
    if a.maximi == b.maximi:
        maximi = a.maximi
        if a.max_cmp == b.max_cmp:
            max_cmp = a.max_cmp
            max_by = __join_opt_strings(a.max_by, b.max_by)
        elif a.max_cmp == operator.gt and b.max_cmp == operator.ge:
            max_cmp = a.max_cmp
            max_by = a.max_by
        elif a.max_cmp == operator.ge and b.max_cmp == operator.gt:
            max_cmp = b.max_cmp
            max_by = b.max_by
        else:
            raise ValueError("Invalid comperators")

    elif a.maximi < b.maximi:
        maximi = b.maximi
        max_by = b.max_by
        max_cmp = b.max_cmp

    elif a.maximi > b.maximi:
        maximi = a.maximi
        max_by = a.max_by
        max_cmp = a.max_cmp

    # Return the result
    return Interval(minimi, maximi, min_cmp, max_cmp,
                    min_by=min_by, max_by=max_by)


def parse_interval(s: str,
                   *,
                   by: Optional[str] = None,
                   min_by: Optional[str] = None,
                   max_by: Optional[str] = None,
                   ) -> Interval:
    """
    Parse a string as an interval.

    :param s: String to parse, should be on form `[<start>, <end>]`,
              where the brackes can be either hard or soft (for closed and
              open intervals), and start and end must both be parsable by
              semver.VersionInfo.parse.
    :param by: Passed verbatim to `Interval`s constructor
    :param min_by: Passed verbatim to `Interval`s constructor
    :param max_by: Passed verbatim to `Interval`s constructor
    """
    min_cmp: Comp
    max_cmp: Comp
    start: VersionInfo
    end: VersionInfo

    if s[0] == '[':
        min_cmp = operator.ge
    elif s[0] == '(':
        min_cmp = operator.gt
    else:
        raise ValueError(f"Invalid start of interval '{s[0]}'")

    start, end = [VersionInfo.parse(x.strip()) for x in s[1:-1].split(',')]

    if s[-1] == ']':
        max_cmp = operator.le
    elif s[-1] == ')':
        max_cmp = operator.lt
    else:
        raise ValueError(f"Invalid end of interval '{s[-1]}'")

    return Interval(start, end, min_cmp, max_cmp,
                    by=by, min_by=min_by, max_by=max_by)
