"""Puppet modules backed by git repos."""

from datetime import datetime
import json
import logging
import os.path
import time
import shutil
import base64

from typing import (
    Any,
    Optional,
)

# from git.repo.base import Repo
# import git
# import gitdb.exc
import pygit2
from pygit2 import (
    Repository,
    GIT_REPOSITORY_OPEN_BARE,
    GIT_OBJ_BLOB,
    GIT_OBJ_COMMIT,
    GIT_OBJ_TREE,
)
from semver import VersionInfo

from r11k.puppetmodule.base import PuppetModule
from r11k.puppet import PuppetMetadata
import r11k.config
from r11k.gitutil import (
    find_remote,
    repo_last_fetch,
    checkout,
)


logger = logging.getLogger(__name__)


def fs_encode(name: str) -> str:
    """Return name transformed to be suitable as a filename."""
    return base64.b64encode(name.encode('UTF-8'), b'-_') \
                 .decode('ASCII')


class GitPuppetModule(PuppetModule):
    """
    Puppet module backed by git repo.

    Three different git repos are handled by this class:
    - The upstream, referenced by the url self.git.
    - The published module, which is given as the argument to publish()
    - Our local cache of the upstream, contained in self.repo_path
      (and self.__repo)

    :param name: If the given module dosen't provide any metadata,
                 then this becomes the name of the published directory
                 for module (and must therefor match what the module
                 is written to be published as). If the module
                 contains metadata.json, the the name will be changed
                 to that, but a warning will be issued on mismatches.
    :param git: URL to a git repo containing a Puppet module
    :param version: Any valid git reference, such as a
                    tag,
                    branch,
                    commit,
                    etc...
    """

    def __init__(self,
                 name: str,
                 git: str,
                 *,
                 version: str,
                 config: r11k.config.Config = r11k.config.config):
        super().__init__(name, version=version, config=config)
        if version == 'HEAD':
            raise ValueError('HEAD is an invalid version refspec')
        self.git = git
        self.__repo: Optional[Repository] = None

    @property
    def repo_path(self) -> str:
        """Return path of local cache of upstream."""
        # using self._og_name would be nice, but will lead to
        # problems if two repos have the same name (which is rather
        # common, if different modules for the same functionallity is
        # used in different part of the puppet deployment).
        cache_name: str = fs_encode(self.git)
        return os.path.join(self.config.clone_base, cache_name)

    @property
    def repo(self) -> Repository:
        """
        Return the coresponding git repo for this module, if available.

        This will be the bare repo placed in the cache directory.

        This method only works if the module have git as its origin.
        Otherwise it will throw a value error.
        """
        if not self.__repo:
            try:
                self.__repo = pygit2.Repository(self.repo_path, GIT_REPOSITORY_OPEN_BARE)
                if self.__repo.remotes['origin'].url != self.git:
                    logger.warning('Cached repo "%s" already exists, but points to "%s" rather than "%s". Replacing.' % (self.name, self.__repo.remotes['origin'].url, self.git))  # noqa: E501
                    shutil.rmtree(self.repo_path)
                    self.__repo = pygit2.clone_repository(self.git, self.repo_path, bare=True)
            except pygit2.GitError:
                self.__repo = pygit2.clone_repository(self.git, self.repo_path, bare=True)

            if self.__repo.is_empty:
                logger.warning('Empty repository cloned %s' % self.git)

        # This fetches updates all remote branches, and then merges
        # the local branches to match remote. Local branches is needed
        # since those are used when this repo gets cloned.
        # TODO what happens with diverged branches?
        self.__repo.remotes['origin'].fetch(refspecs=['refs/heads/*:refs/heads/*'])

        return self.__repo

    def publish(self, path: str) -> None:
        """Publish this module by cloning it to path."""
        # This forces the download of upstream, which is needed
        # to publish it.
        assert self.repo
        assert self.version

        try:
            repo = pygit2.Repository(path)
            # TODO default origin name
            if repo.remotes['origin'].url == self.repo_path:
                repo.remotes['origin'].fetch(['+refs/heads/*:refs/heads/*'])
            else:
                logger.warning('Collision when publishing "%s", expected remote "%s", got %s". Replacing' % (self.name, self.repo_path, repo.remotes['origin'].url))  # noqa: E501
                shutil.rmtree(path)
                repo = pygit2.clone_repository(self.repo_path, path)
        except pygit2.GitError:
            # TODO Would replacing self.repo_path to self.repo.path
            #      allow us to remove the assert above?
            repo = pygit2.clone_repository(self.repo_path, path)

        # NOTE force without a warning isn't the best idea, but will
        # have to do for now.
        checkout(repo, self.version, force=True)
        logger.info('Published module %s', self)

    def versions(self) -> list[VersionInfo]:
        """
        Return all tags looking like release versions in the repo.

        Note that any valid ref resolving to a commit is a valid
        version, and can be specified in the puppetfile. (for example:
        master, or a commit hash).
        """
        versions: list[VersionInfo] = []
        pre = 'refs/tags/'
        for tag in self.repo.references:
            if not tag.startswith(pre):
                continue
            # if not tag.name:
            #     raise ValueError("Can't have empty tag")
            # name: str = tag.name
            name = tag[len(pre):]
            if name[0] == 'v':
                name = name[1:]
            try:
                versions.append(VersionInfo.parse(name))
            except ValueError:
                pass

        return sorted(versions)

    def _fetch_metadata(self) -> PuppetMetadata:
        """Fetch metadata for this module."""
        """Clone module form git url."""
        name = self.name
        version = self.version or self.latest()
        git_url = self.git

        repo = self.repo

        remote = find_remote(repo.remotes, git_url)
        if not remote:
            raise ValueError('Existing repo, but with different remote')

        last_modified = repo_last_fetch(repo)
        now = datetime.utcfromtimestamp(time.time())

        if abs(now - last_modified).seconds > self.config.git_ttl:
            remote.fetch(['+refs/heads/*:refs/heads/*'])

        # tree should have the type Optional[git.Tree], but the mypy
        # version available at Arch Linux can't resolve it.
        #     arch (2022-10-03) $ mypy --version
        #     mypy 0.981 (compiled: no)
        #     venv $ mypy --version
        #     mypy 0.971 (compiled: yes)
        tree: Optional[Any] = None

        # NOTE this this always prepends/removes a 'v' from version,
        # which will get weird with non-semver versions, e.g. HEAD
        # becomes [HEAD, vHEAD].
        for alt in tag_alternatives(version):
            try:
                tree = repo.revparse(alt).from_object.peel(GIT_OBJ_TREE)
                if tree.type != GIT_OBJ_TREE:
                    raise RuntimeError(f"Reference '{alt}' doesn't reference a commit in {git_url}")
            except KeyError:
                pass

        # An empty tree is falsy. Only check for the non-existance here
        if tree is None:
            raise RuntimeError(f'No tree for version {version}')

        try:
            # blob = tree.join('metadata.json')
            blob = tree['metadata.json']
            if blob.type != GIT_OBJ_BLOB:
                raise RuntimeError('metadata.json not a blob')
            data = blob.data
            # checksum, type, len, file = blob.data_stream
            # data = file.read()
            return PuppetMetadata(**json.loads(data))
        except KeyError:
            logger.info('No metadata for %s, generating default', name)
            return PuppetMetadata(name=name,
                                  version=version,
                                  author='UNKNOWN',
                                  license='UNKNOWN',
                                  summary='-',
                                  source=git_url,
                                  dependencies=[])

    def serialize(self) -> dict[str, Any]:
        """
        Serialize back into form from puppetfile.yaml.

        The git version also includes the git url.
        """
        return {**super().serialize(),
                'version': self.repo.revparse(self.version).from_object.peel(GIT_OBJ_COMMIT).hex,
                'git': self.git}

    def __repr__(self) -> str:
        return f"GitPuppetModule('{self.name}', '{self.version}')"


def tag_alternatives(name: str) -> list[str]:
    """
    Return the given tag name with and without leading 'v'.

    Assumes that tag is either on the from
    `v1.0.0` or `1.0.0`.
    :return: `['1.0.0', 'v1.0.0']`
             (non-prefixed version always being first).
    """
    if not name:
        raise ValueError("Can't have empty tag string")
    if name[0] == 'v':
        return [name[1:], name]
    else:
        return [name, f'v{name}']
