"""
Modules form the [Puppet Forge][forge].

The Forge notes a few installation methods, where we (currently)
aren't listed. The manual install method

```sh
puppet module install puppetlabs-stdlib --version 8.4.0
```
maps unto us as
>>> ForgePuppetModule(name='puppetlabs-stdlib', version='8.4.0')

[forge]: https://forge.puppet.com/
"""

import logging
import os.path
import hashlib
import threading
import shutil

from semver import VersionInfo
import tarfile

from r11k.puppetmodule.base import PuppetModule
from r11k.puppet import PuppetMetadata
from r11k.forge import FullForgeModule, CurrentRelease
from r11k import util
from r11k.util import (
    unfix_name,
)


logger = logging.getLogger(__name__)


class ForgePuppetModule(PuppetModule):
    """
    Puppet module backed by the Puppet Forge.

    :param name: Module name, as noted on the Forge for installation.
    :param version: Semver formatted string, as per the Puppet Forge
    """

    def _fetch_metadata(self) -> PuppetMetadata:
        """Return metadata for this module."""
        try:
            version = self.version or self.latest()
            return self.get_versioned_forge_module_metadata(self.name, version).metadata
        except Exception as e:
            logger.error(self)
            raise e

    def _fetch_latest_metadata(self) -> FullForgeModule:
        """
        Get complete metadata for module.

        Possibly download the module from puppet forge, or simple look it
        up in the module cache.

        :param module_name: Name of the module to look up.
        """
        unfixed_module_name = unfix_name(self.name)

        url = f'{self.config.api_base}/v3/modules/{unfixed_module_name}'
        data = self.config.httpcache.get(url)
        if not data:
            raise ValueError('No data')

        return FullForgeModule(**data)

    def fetch(self) -> str:
        """
        Download module from puppet forge.

        Return path to a tarball.
        """
        name = self.name
        version = self.version

        # TODO this should use the forges file_uri instead (when available)
        url = f"{self.config.api_base}/v3/files/{name}-{version}.tar.gz"

        # TODO this should use the HTTP cache, but currently HTTPcache
        #      is hardcoded to expect JSON responses
        filename = os.path.join(self.config.tar_cache, f'{name}-{version}.tar.gz')

        if not os.path.exists(filename):
            util.download_file(url, filename)

        return filename

    def publish(self, path: str) -> None:
        """
        Extract this module to path.

        Does some checks if the source and target differs, and does
        nothing if they are the same.
        """
        tar_filepath = self.fetch()
        with tarfile.open(tar_filepath) as archive:
            archive_root = archive.next()
            if not archive_root:
                raise ValueError(f"Empty tarfile: {tar_filepath}")
            if not archive_root.isdir():
                raise ValueError(f"Tar root not a directory: {tar_filepath}, {archive_root.name}")

            unlink_old: bool = False
            if os.path.exists(path):
                if os.path.isdir(path):
                    with open(os.path.join(path, 'metadata.json'), 'rb') as f:
                        file_meta_chk = hashlib.sha256(f.read())

                    member = archive.getmember(archive_root.name + '/metadata.json')
                    if not archive.fileobj:
                        raise ValueError('Underlying tar-file non-existant')
                    archive.fileobj.seek(member.offset_data)
                    tar_meta_chk = hashlib.sha256(archive.fileobj.read(member.size))

                    if tar_meta_chk == file_meta_chk:
                        return
                    unlink_old = True
                else:
                    os.unlink(path)

            archive.extractall(path=os.path.dirname(path))
            extract_path = os.path.join(os.path.dirname(path),
                                        archive_root.name)
            tmp_path = os.path.join(os.path.dirname(path), '.old-' + os.path.basename(path))
            # This will hopefully cause the update to be
            # unnoticable to any observers
            with threading.Lock():
                if unlink_old:
                    os.rename(path, tmp_path)
                os.rename(extract_path, path)
            if os.path.exists(tmp_path):
                shutil.rmtree(tmp_path)

    def versions(self) -> list[VersionInfo]:
        """Return available versions."""
        j = self._fetch_latest_metadata()
        # return [o['version'] for o in j['releases']]
        return [o.version for o in j.releases]

    def __repr__(self) -> str:
        return f"ForgePuppetModule('{self.name}', '{self.version}')"

    def get_versioned_forge_module_metadata(self, module_name: str, version: str) -> CurrentRelease:
        """Retrieve release data from puppet forge."""
        unfixed_module_name = unfix_name(module_name)
        url = f'{self.config.api_base}/v3/releases/{unfixed_module_name}-{version}'
        data = self.config.httpcache.get(url)
        if not data:
            raise ValueError('No Data')
        return CurrentRelease(**data)
