"""
Abstract base class for Puppet module sources.

Puppet modules can come from many places. Here we provide a common
interface for how the rest of the program should be able to interact
with them. A few implementation parts are here, for things that will
be common among almost all module providers.
"""

from typing import (
    Any,
    Optional,
)
from semver import VersionInfo
from r11k.puppet import PuppetMetadata
import r11k.config


class PuppetModule:
    r"""
    Abstract base class for all puppet modules.

    Each implementation *must* implement the following:
    - `_fetch_metadata`
    - `publish`
    - `versions`

    ##### Protected fields
    ###### `_fetch_metadata`
    A method which retrieves the metadata from *somewhere*. The
    property `metadata` by default caches this information.

    :param name: Name of the module. Further meaning depends on the
                 implementation.
    :param version: Version to use. Valid values depend on the
                    implementation.
    :param config: Runtime configuration which may be used by
                   implementations to aid them in retrieving a module.
    """

    # pragma: no cover
    def __init__(self,
                 name: str,
                 *,
                 version: Optional[str] = None,
                 config: r11k.config.Config = r11k.config.config):
        self.name = name
        self._og_name = name
        self.version: Optional[str] = version
        self._metadata: Optional[PuppetMetadata] = None
        self.config = config
        self.explicit: bool = False

    @property
    def metadata(self) -> PuppetMetadata:
        """
        Property containing the modules metadata.

        The metadata almost always needs to be fetched from an
        external source. Each implementation must implement
        `_fetch_metadata` which solves this, while this property
        ensures that `_fetch_metadata` is only called once.
        """
        if not self._metadata:
            self._metadata = self._fetch_metadata()
        return self._metadata

    def _fetch_metadata(self) -> PuppetMetadata:  # pragma: no cover
        """Fetch metadata for a given module."""
        raise NotImplementedError()

    def publish(self, path: str) -> None:  # pragma: no cover
        """
        Publish this module to the given path.

        [Parameters]
            path - absolute path to modules own directory, for example:
                   /etc/puppetlabs/code/environments/{env_name}/modules/{module_name}

        :raises: `NotImplementedError`
        """
        raise NotImplementedError()

    def versions(self) -> list[VersionInfo]:  # pragma: no cover
        """
        List available versions of this module.

        :raises: `NotImplementedError`
        """
        raise NotImplementedError()

    def latest(self) -> VersionInfo:
        """Return the latest available version."""
        return max(self.versions())

    @property
    def module_path(self) -> str:
        """Where module is located in file system."""
        parts = self.name.split('-', 1)
        if len(parts) == 1:
            return parts[0]
        else:
            return parts[1]

    def serialize(self) -> dict[str, Any]:
        """
        Serialize back into form from puppetfile.yaml.

        Includes name, and version if set.
        """
        return {
            'name': self.name,
            **({'version': self.version}
               if self.version else {})
        }

    def __repr__(self) -> str:  # pragma: no cover
        return f"PuppetModule('{self.name}', '{self.version}')"
