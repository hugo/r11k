"""
Datatypes for requested Puppet modules.

An abstract base class which implements our expected interface is
available in `r11k.puppetmodule.base`. Every other submodule here
should be an implementation, providing a source for where to get
Puppet modules.

This is what is stored in puppetfile.yaml

The implementations are in
- `r11k.puppetmodule.git`
- `r11k.puppetmodule.forge`
"""

from .base import PuppetModule
from .git import GitPuppetModule
from .forge import ForgePuppetModule
from typing import (
    Literal,
)


def parse_module_dict(d: dict) -> PuppetModule | Literal['remove']:
    """
    Parse dict describing module into module object.

    Currently, it becomes a `GitPuppetModule` if the `git` key is
    present, and a `ForgePuppetModule` otherwise.
    """
    if 'remove' in d and d['remove']:
        return 'remove'
    elif 'git' in d:
        return GitPuppetModule(**d)
    else:
        return ForgePuppetModule(**d)
