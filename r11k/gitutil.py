"""Various minor utilities for git repos."""

from datetime import datetime
import os
import os.path
import subprocess
from typing import (
    Optional,
)

import pygit2


def repo_last_fetch(repo: pygit2.Repository) -> datetime:
    """
    When was a fetch last perfermed on this repo.

    :raises: `FileNotFoundError` if no suitable HEAD could be found.
    """
    files = [
        os.path.join(repo.path, 'FETCH_HEAD'),
        os.path.join(repo.path, 'HEAD'),
    ]
    for fetch_head in files:
        try:
            head_stat = os.stat(fetch_head).st_mtime
            return datetime.utcfromtimestamp(head_stat)
        except FileNotFoundError:
            pass
    raise FileNotFoundError(f"No suitable head for repo {repo}")


def find_remote(remotes: pygit2.remote.RemoteCollection,
                target_url: str) -> Optional[pygit2.remote.Remote]:
    """Find the remote matching the given url."""
    for remote in remotes:
        if target_url == remote.url:
            return remote
    return None


def checkout(repo: pygit2.Repository,
             refspec: str,
             force: bool = False) -> None:
    """
    Run gits porecalin checkout.

    Shells out to git checkout.

    git-checkout(1) allows us to give a reference on any form,
    including a commit hash, and a non-local branch (e.g.
    origin/master). Proper checkout would be something like
    >>> repo.checkout(f'refs/remotes/origin/{self.version}')
    >>> repo.checkout(self.version)
    """
    cmdline = ['git', 'checkout', '--quiet', refspec]
    if force:
        cmdline += ['--force']
    subprocess.run(cmdline,
                   check=True,
                   cwd=repo.workdir)
