"""Extension of `semver` for parsing Puppet version specifications."""

import operator
import re

from typing import (
    Callable,
    Tuple,
)
try:  # pragma: nocover
    from typing import TypeAlias  # type: ignore
except ImportError:  # pragma: nocover
    from typing import Any as TypeAlias  # type: ignore

from semver import VersionInfo

Comp: TypeAlias = Callable[[VersionInfo, VersionInfo], bool]

min_version: VersionInfo = VersionInfo(major=0)
max_version: VersionInfo = VersionInfo(major=2**63)


def parse_version(version: str) -> Tuple[Comp, VersionInfo, Comp, VersionInfo]:
    """
    Parse a puppet version.

    Specification for the format can be found at, but is also
    sumarized in the version parameter.
    https://puppet.com/docs/puppet/7/modules_metadata.html#metadata-version

    The returning procedures min_comperator and max_comperator are
    both procedures taking two arguments: the value to compare
    against, and their coresponding returning version. So to test a
    version v, the correct usage would be
        min_comperator(v, min_version) and max_comperator(v, max_version)

    :param version: should be on off
    - `<a>.<b>.<c>`    Exact version
    - `<a>.x`          Exact major
    - `<a>.<b>.x`      Exact minor
    - `>= <a>.<b>.<c>` GE specified
    - `<= <a>.<b>.<c>` LE specifiec
    - `> <a>.<b>.<c>`  GT specified
    - `< <a>.<b>.<c>`  LT specifiec
    - `>= <a>.<b>.<c> < <d>.<e>.<f>` GE *a*.*b*.*c*, LT *d*.*e*.*f*

    :return: A 4-tuple containing
    - min_comperator
    - min_version
    - max_comperator
    - max_version
    """
    if m := re.match(r'^(\d+)[.](\d+)[.](\d+)$', version):
        min = VersionInfo(m[1], m[2], m[3])
        max = min
        return (operator.ge, min, operator.le, max)

    elif m := re.match(r'^(\d+)[.]x$', version):
        min = VersionInfo(m[1], 0, 0)
        max = min.bump_major()
        return (operator.ge, min, operator.lt, max)

    elif m := re.match(r'^(\d+)[.](\d+)[.]x$', version):
        min = VersionInfo(m[1], m[2], 0)
        max = min.bump_minor()
        return (operator.ge, min, operator.lt, max)

    elif m := re.match(r'^([<>]=?) (\d+[.]\d+[.]\d+)$', version):
        if m[1] == '<':
            v = VersionInfo.parse(m[2])
            return (operator.gt, min_version, operator.lt, v)
        elif m[1] == '<=':
            v = VersionInfo.parse(m[2])
            return (operator.gt, min_version, operator.le, v)
        elif m[1] == '>':
            v = VersionInfo.parse(m[2])
            return (operator.gt, v, operator.lt, max_version)
        else:  # m[1] == '>=':
            v = VersionInfo.parse(m[2])
            return (operator.ge, v, operator.lt, max_version)

    elif m := re.match(r'^>= ?(\d+[.]\d+[.]\d+) ?< ?(\d+[.]\d+[.]\d+)$',
                       version):
        min = VersionInfo.parse(m[1])
        max = VersionInfo.parse(m[2])
        return (operator.ge, min, operator.lt, max)

    else:
        raise ValueError('Invalid version:', version)
